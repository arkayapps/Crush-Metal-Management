/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package crushmetalmanagement;

import crushmetalmanagement.cmbeans.CustomerMaster;
import crushmetalmanagement.cmbeans.Product;
import crushmetalmanagement.cmbeans.ProductUnit;
import crushmetalmanagement.dao.CashRegisterMasterDAO;
import crushmetalmanagement.dao.CreditRegisterMasterDAO;
import crushmetalmanagement.dao.CrushMetalConnection;
import crushmetalmanagement.dao.CustomeMasterDAO;
import crushmetalmanagement.dao.CustomerSiteDAO;
import java.util.List;

/**
 *
 * @author Acer
 */
public interface CrushMetalListener {
    public List<CustomerMaster> getCustomers();
    public List<Product> getProducts();
    public List<ProductUnit> getProductUnits();
    public CrushMetalConnection getCrushMetalConnection();
    public void setCustomers(List<CustomerMaster> customerMasters);
    public String getProductName(int productID);
    public String getProductUnitName(int productUnitID);
    
    public CustomerSiteDAO getCustomerSiteDAO();
    public CustomeMasterDAO getCustomerMasterDAO();
    public CashRegisterMasterDAO getCashRegisterMasterDAO();
    public CreditRegisterMasterDAO getCreditRegisterMasterDAO();
}
