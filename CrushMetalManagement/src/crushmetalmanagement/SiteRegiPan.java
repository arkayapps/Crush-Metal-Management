/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SiteRegiPan.java
 *
 * Created on Jun 28, 2010, 11:24:59 PM
 */
package crushmetalmanagement;

import crushmetalmanagement.cmbeans.CustomerMaster;
import crushmetalmanagement.cmbeans.CustomerSite;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Acer
 */
public class SiteRegiPan extends javax.swing.JPanel {

    private CrushMetalListener crushMetailListener;
    /** Creates new form SiteRegiPan */
    public SiteRegiPan(CrushMetalListener crushMetailListener) {
        initComponents();
        this.crushMetailListener = crushMetailListener;
        this.refreshScreen();
    }

    public void refreshScreen() {
        comCusName.removeAllItems();
        for(CustomerMaster cusMas : crushMetailListener.getCustomers()){
            comCusName.addItem(cusMas.getCustomerName());
        }

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        comCusName = new javax.swing.JComboBox();
        txtSiteName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtAreaSiteDesc = new javax.swing.JTextArea();
        butCreate = new javax.swing.JButton();

        setName("siteRegiPan"); // NOI18N

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder("Add New Customer Site")));

        jLabel1.setText("Customer Name:");

        jLabel2.setText("Site Name:");

        txtSiteName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSiteNameKeyReleased(evt);
            }
        });

        jLabel3.setText("Description:");

        txtAreaSiteDesc.setColumns(20);
        txtAreaSiteDesc.setRows(5);
        jScrollPane1.setViewportView(txtAreaSiteDesc);

        butCreate.setText("Create");
        butCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butCreateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(butCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(comCusName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtSiteName)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE))))
                .addContainerGap(120, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comCusName, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSiteName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(jLabel3))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(butCreate)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtSiteNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSiteNameKeyReleased
        // TODO add your handling code here:
        char k = evt.getKeyChar();

        if (txtSiteName.getText().length() >= 1) {
            if (k >= 47 && k <= 57) {
                txtSiteName.setText(txtSiteName.getText().substring(0, txtSiteName.getText().length() - 1));
            } else {
            }

        }
        if (txtSiteName.getText().length() >= 40) {
            txtSiteName.setText(txtSiteName.getText().substring(0, txtSiteName.getText().length() - 1));
        }
}//GEN-LAST:event_txtSiteNameKeyReleased

    private void butCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butCreateActionPerformed
        String siteName;
        //Check Site Name
        if (txtSiteName.getText().length() == 0) {
            JOptionPane.showMessageDialog(this, "Please enter customer site name.", "Invalid Input", JOptionPane.ERROR_MESSAGE);
            return;
        } else {
            siteName = txtSiteName.getText().toUpperCase();
            if (siteName.length() >= 40) {
                JOptionPane.showMessageDialog(this, "Site Name is too Long", "Invalid Input", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        //Find Customer Id
        int custId = crushMetailListener.getCustomers().get(comCusName.getSelectedIndex()).getCustomerID();
        for(CustomerSite customerSite : crushMetailListener.getCustomerMasterDAO().getCustomerOwnerSites(custId)){
            String sName = customerSite.getSiteName();
                if (sName.equalsIgnoreCase(siteName)) {
                    JOptionPane.showMessageDialog(this, "Dublicate site Name for same customer", "Invalid Input", JOptionPane.WARNING_MESSAGE);
                    return;
                }
        }
        // insert into database
        CustomerSite custSite  = new CustomerSite(siteName, txtAreaSiteDesc.getText(), custId);
       boolean ans = crushMetailListener.getCustomerSiteDAO().addNewCustomerSite(custSite);
       if(ans == true){
           JOptionPane.showMessageDialog(this, "New site added successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
            crushMetailListener.getCustomerMasterDAO().getCustomerSite().add(custSite);
            int index=0;
            for(CustomerMaster cust : crushMetailListener.getCustomers()){
                if(cust.getCustomerID()==custSite.getCustomerID()){
                break;
                }
                index++;
            }
           // index--;
            crushMetailListener.getCustomers().get(index).getCustomerSites().add(custSite);
            System.out.println("add"+index);
        }
}//GEN-LAST:event_butCreateActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton butCreate;
    private javax.swing.JComboBox comCusName;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtAreaSiteDesc;
    private javax.swing.JTextField txtSiteName;
    // End of variables declaration//GEN-END:variables
    
}
