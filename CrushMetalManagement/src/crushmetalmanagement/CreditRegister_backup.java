package crushmetalmanagement;

import crushmetalmanagement.cmbeans.CustomerMaster;
import crushmetalmanagement.cmbeans.CustomerSite;
import crushmetalmanagement.cmbeans.Product;
import crushmetalmanagement.cmbeans.ProductUnit;
import crushmetalmanagement.dao.CrushMetalConnection;
import crushmetalmanagement.dao.CustomeMasterDAO;
import crushmetalmanagement.dao.ProductDAO;
import crushmetalmanagement.dao.ProductUnitDAO;
import java.awt.event.KeyEvent;
import java.sql.Date;

import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class CreditRegister_backup extends javax.swing.JFrame {

    private List<Product> products;
    private List<ProductUnit> productUnits;
    private List<CustomerMaster> customers;
    private List<CustomerSite> customerSite;

    private int creditRegisterId;
    private Date creditTranDate;
    private String creditChallanNo;
    private String creditCusId;
    private String creditCusUnloadingPlace;
    private int productNameID;
    private Double creditTranQty;
    private int productUnitId;
    private Double productRate;
    private Double creditTranRoyaltyMT;
    private String creditVehicleNo;
    private String creditDriverName;
    private String creditTranRoyaltyNo;
    private Double creditRoyaltyRate;
    private int royaltyHolderCusID;
    private CrushMetalConnection cmc;

    public CreditRegister_backup() {
        initComponents();

        
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        dateChooserDialog1 = new datechooser.beans.DateChooserDialog();
        dateChooserDialog2 = new datechooser.beans.DateChooserDialog();
        dateChooserDialog3 = new datechooser.beans.DateChooserDialog();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtChaNo = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        dataCho = new datechooser.beans.DateChooserCombo();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        comCusName = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        comSiteName = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        txtUnloadPla = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel22 = new javax.swing.JLabel();
        comRoyaltyName = new javax.swing.JComboBox();
        jPanel4 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        comPrdName = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        txtQty = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        comUnit = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        txtRs = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtPrdRate = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtVhlNo = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        butCreate = new javax.swing.JButton();
        butClose = new javax.swing.JButton();
        butClear = new javax.swing.JButton();
        txtAmount = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txtRoyaltyNo = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtRolMT = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtRoyRate = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtRoyRs = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        jMenuItem3 = new javax.swing.JMenuItem();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Daily Register Entry");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 16));
        jLabel1.setText("Daily Credit Register Entry");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Select Register Type"));

        jLabel7.setText("Challan No:");

        txtChaNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtChaNoKeyReleased(evt);
            }
        });

        jLabel8.setText("Select Date:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(dataCho, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(txtChaNo, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(529, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dataCho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(txtChaNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Customer"));

        jLabel3.setText("Select Customer:");

        comCusName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comCusNameFocusGained(evt);
            }
        });

        jLabel5.setText("Select Customer Site:");

        comSiteName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comSiteNameActionPerformed(evt);
            }
        });
        comSiteName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comSiteNameFocusGained(evt);
            }
        });

        jLabel6.setText("Unloading Place:");

        txtUnloadPla.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUnloadPlaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUnloadPlaKeyReleased(evt);
            }
        });

        jButton1.setText("New");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("New");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel22.setText("Royalty Name:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5))
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(comSiteName, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comCusName, javax.swing.GroupLayout.Alignment.TRAILING, 0, 296, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE))
                .addGap(32, 32, 32)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22))
                .addGap(19, 19, 19)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comRoyaltyName, 0, 231, Short.MAX_VALUE)
                    .addComponent(txtUnloadPla, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(171, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(comCusName, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(comSiteName, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton2)
                            .addComponent(jLabel5))
                        .addGap(40, 40, 40))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jLabel22)
                            .addComponent(comRoyaltyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtUnloadPla, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(41, 41, 41)))
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Product Description"));

        jLabel9.setText("Select Product:");

        comPrdName.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "10 M.M.", "20 M.M.", "40 M.M", "10-20 M.M.", "10-40 M.M.", "20-40M.M.", "06 M.M.", "DUST", "WMM", "40/63", "63/90", "STONE", "PUAR" }));
        comPrdName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comPrdNameActionPerformed(evt);
            }
        });

        jLabel10.setText("Quantity:");

        txtQty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtQtyFocusLost(evt);
            }
        });
        txtQty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtQtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtQtyKeyReleased(evt);
            }
        });

        jLabel13.setText("Units:");

        comUnit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "CFT", "CMT", "TON" }));

        jLabel16.setText("Rs.");

        txtRs.setEnabled(false);
        txtRs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRsKeyReleased(evt);
            }
        });

        jLabel19.setText("Product Rate");

        txtPrdRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrdRateActionPerformed(evt);
            }
        });
        txtPrdRate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPrdRateFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addGap(18, 18, 18)
                .addComponent(comPrdName, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtQty, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel13)
                .addGap(18, 18, 18)
                .addComponent(comUnit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jLabel19)
                .addGap(18, 18, 18)
                .addComponent(txtPrdRate, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtRs, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(195, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(comPrdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(txtQty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(comUnit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(txtPrdRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(txtRs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Vehicle Number"));

        jLabel12.setText("Vehicle Number:");

        txtVhlNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtVhlNoKeyReleased(evt);
            }
        });

        jLabel15.setText("Driver Name:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtVhlNo, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(702, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtVhlNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(29, Short.MAX_VALUE))
        );

        butCreate.setText("Create");
        butCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butCreateActionPerformed(evt);
            }
        });

        butClose.setText("Close");
        butClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butCloseActionPerformed(evt);
            }
        });

        butClear.setText("Clear");
        butClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butClearActionPerformed(evt);
            }
        });

        txtAmount.setFont(new java.awt.Font("Tahoma", 0, 18));
        txtAmount.setEnabled(false);
        txtAmount.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtAmountFocusGained(evt);
            }
        });

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Royalty Information"));

        jLabel11.setText("Royalty Number:");

        txtRoyaltyNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRoyaltyNoKeyReleased(evt);
            }
        });

        jLabel14.setText("Royalty M.T:");

        txtRolMT.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtRolMTFocusLost(evt);
            }
        });
        txtRolMT.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRolMTKeyReleased(evt);
            }
        });

        jLabel18.setText("Rate:");

        txtRoyRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRoyRateActionPerformed(evt);
            }
        });
        txtRoyRate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtRoyRateFocusLost(evt);
            }
        });
        txtRoyRate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRoyRateKeyReleased(evt);
            }
        });

        jLabel21.setText("Royalty Rate");

        txtRoyRs.setEnabled(false);
        txtRoyRs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRoyRsKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtRoyaltyNo, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtRolMT, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel18)
                .addGap(18, 18, 18)
                .addComponent(txtRoyRate, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel21)
                .addGap(18, 18, 18)
                .addComponent(txtRoyRs, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(347, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtRoyaltyNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(txtRolMT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(txtRoyRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(txtRoyRs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        jLabel20.setText("Total Amount:");

        jMenu1.setText("File");
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        jMenuItem1.setText("Create");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Clear");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);
        jMenu1.add(jSeparator1);

        jMenuItem3.setText("Close");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(429, 429, 429)
                        .addComponent(jLabel1))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(86, 86, 86)
                                .addComponent(txtAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                                .addGap(308, 308, 308)
                                .addComponent(butCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(butClear, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(butClose, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(134, 134, 134))
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(51, 51, 51))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel20)
                .addContainerGap(1024, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jLabel20))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(butCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(butClear, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(butClose, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(45, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void butCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butCloseActionPerformed
        // TODO add your handling code here:
        //this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_butCloseActionPerformed

    private void butClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butClearActionPerformed
        // TODO add your handling code here:
        txtChaNo.setText("");
        // txtCusName.setText("");
        txtQty.setText("");
        txtRoyaltyNo.setText("");
        txtUnloadPla.setText("");
        txtVhlNo.setText("");
    }//GEN-LAST:event_butClearActionPerformed

    private void butCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butCreateActionPerformed


        java.util.Date cashRegisterDate;

        creditChallanNo = txtChaNo.getText();
        productNameID = products.get(comPrdName.getSelectedIndex()).getProductNameId();
        royaltyHolderCusID = customers.get(comRoyaltyName.getSelectedIndex()).getCustomerID();
        productUnitId = productUnits.get(comUnit.getSelectedIndex()).getPrdUnitId();
//        cashReceiveBy = txtreceive.getText();
//        cashVehicleNo = txtVhlNo.getText();
//        cashDriverName = txtDriverName.getText();
//        cashReceiveBy = txtreceive.getText();

        int chlNo;
        java.sql.Date date;
        java.util.Date uDate;
        int cusId = 0;
        int siteId = 0;
        String cusName;
        String place;
        String prdName = comPrdName.getSelectedItem().toString();
        String units = comUnit.getSelectedItem().toString();
        String vhlNo;
        String rolNo;
        float qty;
        float mt;
        float rsInr;
        float prdPrice = 0;
        float royPrice = 0;
        String receivedBy;
        //Qty code
        if (txtQty.getText().length() == 0) {
            JOptionPane.showMessageDialog(this, "Please Enter Product Quantity", "Invalid Input", JOptionPane.WARNING_MESSAGE);
            return;
        } else {
            try {
                qty = Float.parseFloat(txtQty.getText());
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Invalid Quantity Value", "Invalid Input", JOptionPane.WARNING_MESSAGE);
                return;
            }
        }
        //Date Code
        if (dataCho.getSelectedDate() != null) {
            uDate = dataCho.getSelectedDate().getTime();
            date = new java.sql.Date(uDate.getYear(), uDate.getMonth(), uDate.getDate());
            System.out.println(date);
        } else {
            JOptionPane.showMessageDialog(this, "Please Enter Date", "Invalid Input", JOptionPane.WARNING_MESSAGE);
            return;
        }
        //Royalty No code
        if (txtRoyaltyNo.getText().length() == 0) {
            rolNo = "";
        } else {
            try {
                rolNo = txtRoyaltyNo.getText();
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Invalid Royalty Value", "Invalid Input", JOptionPane.WARNING_MESSAGE);
                return;
            }
        }




        //Unloading Place Code
        if (txtUnloadPla.getText().length() == 0) {
            place = "";
        } else {
            place = txtUnloadPla.getText();
            if (place.length() >= 60) {
                JOptionPane.showMessageDialog(this, "Too Long Place Name", "Invalid Input", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        //Vehicle Number Code
        if (txtVhlNo.getText().length() == 0) {
            vhlNo = "";
        } else {
            vhlNo = txtVhlNo.getText();
        }
        // Rs
        if (txtRs.getText().length() == 0) {
            rsInr = 0;
        } else {
            rsInr = Float.parseFloat(txtRs.getText());
        }
 
        //Cash & Challan Code
        //  if (comRegType.getSelectedIndex() == 0) { // if create Chllan is selected
        //check challan number
        if (txtChaNo.getText().length() == 0) {
            JOptionPane.showMessageDialog(this, "Please Enter Challan No", "Invalid Input", JOptionPane.WARNING_MESSAGE);
            return;
        } else {
            try {
                chlNo = Integer.parseInt(txtChaNo.getText());
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Invalid Chalaln No", "Invalid Input", JOptionPane.WARNING_MESSAGE);
                return;
            }
        }
        //chk customer Name
        if (comCusName.getSelectedIndex() == -1 || comSiteName.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Please select Customer Name OR Customer Site", "Invalid Input", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (txtPrdRate.getText().length() <= 0) {
            JOptionPane.showMessageDialog(this, "Please Enter Product Rate", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        } else {
            prdPrice = Float.parseFloat(txtPrdRate.getText());
        }
        if (txtRoyaltyNo.getText().length() > 0) {
            if (txtRolMT.getText().length() <= 0) {
                JOptionPane.showMessageDialog(this, "Please Enter Royalty MT", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            } else {
                //txtRolMT = Float.parseFloat(txtRolMT.getText());
            }
        }
        if (txtRoyaltyNo.getText().length() > 0) {
            if (txtRoyRate.getText().length() <= 0) {
                JOptionPane.showMessageDialog(this, "Please Enter Royalty Rate", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            } else {
                royPrice = Float.parseFloat(txtRoyRate.getText());
            }
        }

        //chk customer Name
        int cusIdr = 0;
        if (comRoyaltyName.getSelectedIndex() == -1 || comRoyaltyName.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Please select Customer Name OR Customer Site", "Invalid Input", JOptionPane.WARNING_MESSAGE);
            return;
        } else // chk input
        {
            System.out.println("Challan No: " + chlNo);
        }
        System.out.println("Reg Date  : " + date);
//                System.out.println("Status    : " + status);
        System.out.println("Cus Id    : " + cusId);
        System.out.println("Site Id   : " + siteId);
        System.out.println("Vhl No    : " + vhlNo);
        System.out.println("Prod Name : " + prdName);
        System.out.println("Prd Qty   : " + qty);
        System.out.println("Prd Unit  : " + units);
        System.out.println("Royalty No: " + rolNo);
        System.out.println("Load Place: " + place);
        System.out.println("Product Price: " + prdPrice);

        // insert into challan Registor


        System.out.println("Reg Date  : " + date);
//            System.out.println("Status    : " + status);
        System.out.println("Cus Id    : " + cusId);
        System.out.println("Site Id   : " + siteId);
        System.out.println("Vhl No    : " + vhlNo);
        System.out.println("Prod Name : " + prdName);
        System.out.println("Prd Qty   : " + qty);
        System.out.println("Prd Unit  : " + units);
        System.out.println("Royalty No: " + rolNo);
        System.out.println("Load Place: " + place);

    }//GEN-LAST:event_butCreateActionPerformed
    //}
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
    }//GEN-LAST:event_formWindowOpened

    private void txtChaNoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtChaNoKeyReleased
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtChaNo.getText().length() >= 1) {
            if (k >= 47 && k <= 57) {
            } else {
                txtChaNo.setText(txtChaNo.getText().substring(0, txtChaNo.getText().length() - 1));
            }
            if (txtChaNo.getText().length() == 11) {
                txtChaNo.setText(txtChaNo.getText().substring(0, txtChaNo.getText().length() - 1));
            }
            if (txtChaNo.getText().length() >= 11) {
                txtChaNo.setText(txtChaNo.getText().substring(0, 10));
            }
        }


    }//GEN-LAST:event_txtChaNoKeyReleased

    private void txtQtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQtyKeyReleased
        // TODO add your handling code here:
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtQty.getText().length() >= 1) {
            if (k >= 47 && k <= 57 || k == '.') {
            } else {
                txtQty.setText(txtQty.getText().substring(0, txtQty.getText().length() - 1));
            }
            if (txtQty.getText().length() == 11) {
                txtQty.setText(txtQty.getText().substring(0, txtQty.getText().length() - 1));
            }
            if (txtQty.getText().length() >= 11) {
                txtQty.setText(txtQty.getText().substring(0, 10));
            }
        }


    }//GEN-LAST:event_txtQtyKeyReleased

    private void txtQtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQtyKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtyKeyPressed

    private void txtVhlNoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVhlNoKeyReleased
        // TODO add your handling code here:
        if (txtVhlNo.getText().length() >= 25) {
            txtVhlNo.setText(txtVhlNo.getText().substring(0, 25));
        }
    }//GEN-LAST:event_txtVhlNoKeyReleased

    private void txtRoyaltyNoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRoyaltyNoKeyReleased
        // TODO add your handling code here:
        if (txtRoyaltyNo.getText().length() >= 9) {
            txtRoyaltyNo.setText(txtRoyaltyNo.getText().substring(0, 9));
        }
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtRoyaltyNo.getText().length() >= 1) {
            if (k >= 47 && k <= 57 || k == '.') {
            } else {
                txtRoyaltyNo.setText(txtRoyaltyNo.getText().substring(0, txtRoyaltyNo.getText().length() - 1));
            }
            if (txtRoyaltyNo.getText().length() == 11) {
                txtRoyaltyNo.setText(txtRoyaltyNo.getText().substring(0, txtRoyaltyNo.getText().length() - 1));
            }
            if (txtRoyaltyNo.getText().length() >= 11) {
                txtRoyaltyNo.setText(txtRoyaltyNo.getText().substring(0, 10));
            }
        }

    }//GEN-LAST:event_txtRoyaltyNoKeyReleased

    private void txtRolMTKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRolMTKeyReleased
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtRolMT.getText().length() >= 1) {
            if (k >= 47 && k <= 57 || k == '.') {
            } else {
                txtRolMT.setText(txtRolMT.getText().substring(0, txtRolMT.getText().length() - 1));
            }
            if (txtRolMT.getText().length() == 7) {
                txtRolMT.setText(txtRolMT.getText().substring(0, txtRolMT.getText().length() - 1));
            }
            if (txtRolMT.getText().length() >= 7) {
                txtRolMT.setText(txtRolMT.getText().substring(0, 7));
            }
        }

    }//GEN-LAST:event_txtRolMTKeyReleased

    private void txtRsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRsKeyReleased
        // TODO add your handling code here:
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtRs.getText().length() >= 1) {
            if (k >= 47 && k <= 57 || k == '.') {
            } else {
                txtRs.setText(txtRs.getText().substring(0, txtRs.getText().length() - 1));
            }
            if (txtRs.getText().length() == 7) {
                txtRs.setText(txtRs.getText().substring(0, txtRs.getText().length() - 1));
            }
            if (txtRs.getText().length() >= 7) {
                txtRs.setText(txtRs.getText().substring(0, 7));
            }
        }
}//GEN-LAST:event_txtRsKeyReleased

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowActivated

    private void comPrdNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comPrdNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comPrdNameActionPerformed

    private void txtRoyRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRoyRateActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_txtRoyRateActionPerformed

    private void txtPrdRateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPrdRateFocusLost
        // TODO add your handling code here:\\\
        float qty;
        float price;
        if (txtQty.getText().length() <= 0) {
            qty = 0f;
        } else {
            qty = Float.parseFloat(txtQty.getText());
        }
        if (txtPrdRate.getText().length() <= 0) {
            price = 0f;
        } else {
            price = Float.parseFloat(txtPrdRate.getText());
        }
        System.out.println(qty * price);
        txtRs.setText("" + qty * price);
        txtAmount.setText("" + qty * price);
        txtRoyRateFocusLost(evt);
}//GEN-LAST:event_txtPrdRateFocusLost

    private void txtQtyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtQtyFocusLost
        // TODO add your handling code here:
        txtPrdRateFocusLost(evt);
    }//GEN-LAST:event_txtQtyFocusLost

    private void txtRoyRateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtRoyRateFocusLost
        // TODO add your handling code here:
        float qty;
        float price;
        if (txtRolMT.getText().length() <= 0) {
            qty = 0f;
        } else {
            qty = Float.parseFloat(txtRolMT.getText());
        }
        if (txtRoyRate.getText().length() <= 0) {
            price = 0f;
        } else {
            price = Float.parseFloat(txtRoyRate.getText());
        }
        System.out.println(qty * price);
        txtRoyRs.setText("" + qty * price);
        float amt = Float.parseFloat(txtRs.getText()) + Float.parseFloat(txtRoyRs.getText());
        txtAmount.setText("" + amt);
}//GEN-LAST:event_txtRoyRateFocusLost

    private void txtRoyRsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRoyRsKeyReleased
        // TODO add your handling code here:
}//GEN-LAST:event_txtRoyRsKeyReleased

    private void txtRolMTFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtRolMTFocusLost
        // TODO add your handling code here:
        txtRoyRateFocusLost(evt);
    }//GEN-LAST:event_txtRolMTFocusLost

    private void txtAmountFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtAmountFocusGained
        // TODO add your handling code here:
        txtAmount.setText("" + (Float.parseFloat(txtRs.getText()) + Float.parseFloat(txtRoyRs.getText())));
}//GEN-LAST:event_txtAmountFocusGained

    private void txtRoyRateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRoyRateKeyReleased
        // TODO add your handling code here:
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtRoyRate.getText().length() >= 1) {
            if (k >= 47 && k <= 57 || k == '.') {
            } else {
                txtRoyRate.setText(txtRoyRate.getText().substring(0, txtRoyRate.getText().length() - 1));
            }
            if (txtRoyRate.getText().length() == 11) {
                txtRoyRate.setText(txtRoyRate.getText().substring(0, txtRoyRate.getText().length() - 1));
            }
            if (txtRoyRate.getText().length() >= 11) {
                txtRoyRate.setText(txtRoyRate.getText().substring(0, 10));
            }
        }
    }//GEN-LAST:event_txtRoyRateKeyReleased

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        butCreateActionPerformed(evt);

    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        butClearActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        SiteRegDia site = new SiteRegDia(this, true);
        site.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        site.setLocationRelativeTo(null);
        site.setVisible(true);
}//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        CusRegDia reg = new CusRegDia(this, true);
        reg.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        reg.setLocationRelativeTo(null);
        reg.setVisible(true);
}//GEN-LAST:event_jButton1ActionPerformed

    private void txtUnloadPlaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUnloadPlaKeyReleased
        // TODO add your handling code here:
        char k = evt.getKeyChar();

        if (txtUnloadPla.getText().length() >= 1) {
            if (k >= 47 && k <= 57) {
                txtUnloadPla.setText(txtUnloadPla.getText().substring(0, txtUnloadPla.getText().length() - 1));
            } else {
            }

        }
        if (txtUnloadPla.getText().length() >= 40) {
            txtUnloadPla.setText(txtUnloadPla.getText().substring(0, txtUnloadPla.getText().length() - 1));
        }
}//GEN-LAST:event_txtUnloadPlaKeyReleased

    private void txtUnloadPlaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUnloadPlaKeyPressed
        // TODO add your handling code here:
}//GEN-LAST:event_txtUnloadPlaKeyPressed

    private void comSiteNameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comSiteNameFocusGained
        // TODO add your handling code here:
}//GEN-LAST:event_comSiteNameFocusGained

    private void comSiteNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comSiteNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comSiteNameActionPerformed

    private void comCusNameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comCusNameFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_comCusNameFocusGained

    private void txtPrdRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrdRateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrdRateActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new CreditRegister_backup().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton butClear;
    private javax.swing.JButton butClose;
    private javax.swing.JButton butCreate;
    private javax.swing.JComboBox comCusName;
    private javax.swing.JComboBox comPrdName;
    private javax.swing.JComboBox comRoyaltyName;
    private javax.swing.JComboBox comSiteName;
    private javax.swing.JComboBox comUnit;
    private datechooser.beans.DateChooserCombo dataCho;
    private datechooser.beans.DateChooserDialog dateChooserDialog1;
    private datechooser.beans.DateChooserDialog dateChooserDialog2;
    private datechooser.beans.DateChooserDialog dateChooserDialog3;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField txtAmount;
    private javax.swing.JTextField txtChaNo;
    private javax.swing.JTextField txtPrdRate;
    private javax.swing.JTextField txtQty;
    private javax.swing.JTextField txtRolMT;
    private javax.swing.JTextField txtRoyRate;
    private javax.swing.JTextField txtRoyRs;
    private javax.swing.JTextField txtRoyaltyNo;
    private javax.swing.JTextField txtRs;
    private javax.swing.JTextField txtUnloadPla;
    private javax.swing.JTextField txtVhlNo;
    // End of variables declaration//GEN-END:variables
}
