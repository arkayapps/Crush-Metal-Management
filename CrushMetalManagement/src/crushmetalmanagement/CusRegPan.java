/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CusRegPan.java
 *
 * Created on Apr 10, 2012, 10:16:41 AM
 */
package crushmetalmanagement;

import crushmetalmanagement.cmbeans.CustomerMaster;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author Acer
 */
public class CusRegPan extends javax.swing.JPanel {

    private CrushMetalListener crushmetalListener;

    /** Creates new form CusRegPan */
    public CusRegPan(CrushMetalListener crushMetalListeners) {
        this.crushmetalListener = crushMetalListeners;
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtCusName = new javax.swing.JTextField();
        txtCusMobile = new javax.swing.JTextField();
        butSave = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtCusPhoneNo = new javax.swing.JTextField();
        txtCusCity = new javax.swing.JTextField();
        txtCusState = new javax.swing.JTextField();
        txtCusZipCode = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtCusAddress = new javax.swing.JTextArea();

        setName("CusRegPan"); // NOI18N

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(crushmetalmanagement.CrushMetalManagementApp.class).getContext().getResourceMap(CusRegPan.class);
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("jPanel1.border.title"))); // NOI18N
        jPanel1.setName("jPanel1"); // NOI18N

        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        txtCusName.setName("txtCusName"); // NOI18N
        txtCusName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCusNameKeyReleased(evt);
            }
        });

        txtCusMobile.setName("txtCusMobile"); // NOI18N
        txtCusMobile.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCusMobileKeyReleased(evt);
            }
        });

        butSave.setText(resourceMap.getString("butSave.text")); // NOI18N
        butSave.setName("butSave"); // NOI18N
        butSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butSaveActionPerformed(evt);
            }
        });

        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N

        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N

        jLabel5.setText(resourceMap.getString("jLabel5.text")); // NOI18N
        jLabel5.setName("jLabel5"); // NOI18N

        jLabel6.setText(resourceMap.getString("jLabel6.text")); // NOI18N
        jLabel6.setName("jLabel6"); // NOI18N

        jLabel7.setText(resourceMap.getString("jLabel7.text")); // NOI18N
        jLabel7.setName("jLabel7"); // NOI18N

        txtCusPhoneNo.setName("txtCusPhoneNo"); // NOI18N

        txtCusCity.setName("txtCusCity"); // NOI18N

        txtCusState.setName("txtCusState"); // NOI18N

        txtCusZipCode.setName("txtCusZipCode"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        txtCusAddress.setColumns(20);
        txtCusAddress.setRows(5);
        txtCusAddress.setName("txtCusAddress"); // NOI18N
        jScrollPane1.setViewportView(txtCusAddress);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtCusCity, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                            .addComponent(txtCusZipCode, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                            .addComponent(txtCusState, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                            .addComponent(txtCusPhoneNo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                            .addComponent(txtCusMobile, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                            .addComponent(txtCusName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                            .addComponent(butSave, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(363, 363, 363))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCusName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(txtCusMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCusPhoneNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(jLabel4))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCusCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCusState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCusZipCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))))
                .addGap(31, 31, 31)
                .addComponent(butSave)
                .addContainerGap(41, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtCusNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCusNameKeyReleased
        // TODO add your handling code here:
        char k = evt.getKeyChar();

        if (txtCusName.getText().length() >= 1) {
            if (k >= 47 && k <= 57) {
                txtCusName.setText(txtCusName.getText().substring(0, txtCusName.getText().length() - 1));
            } else {
            }

        }
        if (txtCusName.getText().length() >= 40) {
            txtCusName.setText(txtCusName.getText().substring(0, txtCusName.getText().length() - 1));
        }
}//GEN-LAST:event_txtCusNameKeyReleased

    private void txtCusMobileKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCusMobileKeyReleased
        // TODO add your handling code here:
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtCusMobile.getText().length() >= 1) {
            if (k >= 47 && k <= 57 || k == '.') {
            } else {
                txtCusMobile.setText(txtCusMobile.getText().substring(0, txtCusMobile.getText().length() - 1));
            }
            if (txtCusMobile.getText().length() == 13) {
                txtCusMobile.setText(txtCusMobile.getText().substring(0, txtCusMobile.getText().length() - 1));
            }
            if (txtCusMobile.getText().length() > 13) {
                txtCusMobile.setText(txtCusMobile.getText().substring(0, 12));
            }
        }
}//GEN-LAST:event_txtCusMobileKeyReleased

    private void butSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butSaveActionPerformed
       // Boolean ans = null;
        int rs = 0;

        if (txtCusName.getText().length() == 0) {
            JOptionPane.showMessageDialog(this, "Please Enter Customer Name:", "Invalid Input", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        String cusName = txtCusName.getText().toUpperCase();
        try {
            for (CustomerMaster str : crushmetalListener.getCustomers()) {
                System.out.println("Cust Info Panal Name" + str.getCustomerName());

                if ((str.getCustomerName()).equalsIgnoreCase(cusName)) {
                    JOptionPane.showMessageDialog(this, "User Already Exist", "error", JOptionPane.ERROR_MESSAGE);
                    txtCusName.setText("");
                    rs++;
                    break;
                }
            }
        } catch (Error sql) {
            JOptionPane.showConfirmDialog(this, "Error in fetch value" + sql, "Error", JOptionPane.ERROR_MESSAGE);
            return;

        }
        if (rs == 0) {
            String mobile = txtCusMobile.getText();
            String phoneno = txtCusPhoneNo.getText();
            String address = txtCusAddress.getText();
            String city = txtCusCity.getText();
            String state = txtCusState.getText();
            String zipcode = txtCusZipCode.getText();
            Boolean isVisible = true;

            System.out.println(mobile + " " + cusName + " " + phoneno + " " + address + " " + city + " " + state + " " + zipcode);
            try {
                CustomerMaster customer = new CustomerMaster(cusName, address, city, state, zipcode, mobile, phoneno, isVisible);
                //        CustomeMasterDAO customerDao = new CustomeMasterDAO(crushmetalListener.getCMC());

                //ans =
                int cusId = crushmetalListener.getCustomerMasterDAO().AddNewCustomer(customer);

                if (cusId != 0) {
                    customer.setCustomerID(cusId);
                    crushmetalListener.getCustomers().add(customer);
                    JOptionPane.showMessageDialog(this, "New Customer added Successfuly", "Success", JOptionPane.INFORMATION_MESSAGE);
                    txtCusMobile.setText("");
                    txtCusName.setText("");
                    txtCusAddress.setText("");
                    txtCusCity.setText("");
                    txtCusMobile.setText("");
                    txtCusPhoneNo.setText("");
                    txtCusState.setText("");
                    txtCusZipCode.setText("");
                } else {
                    JOptionPane.showMessageDialog(this, "Error in insert value", "error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Error sql) {
                JOptionPane.showConfirmDialog(this, "Error in fetch value" + sql, "Error", JOptionPane.ERROR_MESSAGE);
                return;

            }


        }
}//GEN-LAST:event_butSaveActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton butSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtCusAddress;
    private javax.swing.JTextField txtCusCity;
    private javax.swing.JTextField txtCusMobile;
    private javax.swing.JTextField txtCusName;
    private javax.swing.JTextField txtCusPhoneNo;
    private javax.swing.JTextField txtCusState;
    private javax.swing.JTextField txtCusZipCode;
    // End of variables declaration//GEN-END:variables

    public void addCustomerListener(CrushMetalManagementView cmv) {
        this.crushmetalListener = cmv;

    }
}
