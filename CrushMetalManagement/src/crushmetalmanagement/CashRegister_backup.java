package crushmetalmanagement;


import crushmetalmanagement.cmbeans.CashRegisterMaster;
import crushmetalmanagement.cmbeans.CustomerMaster;
import crushmetalmanagement.dao.ProductDAO;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.JOptionPane;
import crushmetalmanagement.cmbeans.Product;
import crushmetalmanagement.cmbeans.ProductUnit;
import crushmetalmanagement.dao.CashRegisterMasterDAO;
import crushmetalmanagement.dao.CrushMetalConnection;
import crushmetalmanagement.dao.CustomeMasterDAO;
import crushmetalmanagement.dao.ProductUnitDAO;
import java.sql.Date;

public class CashRegister_backup extends javax.swing.JFrame {

    private List<Product> products;
    private List<ProductUnit> productUnits;
    private List<CustomerMaster> customers;

    private Date cashTranDate;
    private String cashChallanNo;
    private String cashCusName;
    private String cashCusUnloadingPlace;
    private int productNameID;
    private Double cashTranQty;
    private int productUnitId;
    private Double productRate;
    private String cashReceiveBy;
    private Double cashTranRoyaltyMT;
    private String cashVehicleNo;
    private String cashDriverName;
    private String cashTranRoyaltyNo;
    private Double cashRoyaltyRate;
    private int royaltyHolderCusID;
    CrushMetalConnection cmc;
    public CashRegister_backup() {
        initComponents();
        cmc  = new CrushMetalConnection();
        SimpleDateFormat sim = new SimpleDateFormat("dd-MM-yyyy");
        dataCho.setDateFormat(sim);
       ProductDAO productdao = new ProductDAO(cmc);
       
       products  = productdao.getAllProduct();
       for(Product product : products){
         System.out.println(products.size());
         comPrdName.addItem(product.getProductName());
       }
       ProductUnitDAO productUnitDao = new ProductUnitDAO(cmc);
       productUnits = productUnitDao.getAllProductUnit();
       for(ProductUnit productUnit : productUnits){
           comUnit.addItem(productUnit.getPrdUnitName());
       }
       CustomeMasterDAO cust= new CustomeMasterDAO(cmc);
       customers = cust.getAllCustomer();
       for(CustomerMaster customer : customers){
           comRoyaltyName.addItem(customer.getCustomerName());
       }

    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtChaNo = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        dataCho = new datechooser.beans.DateChooserCombo();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtCusName = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtUnloadPla = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        comRoyaltyName = new javax.swing.JComboBox();
        jPanel4 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        comPrdName = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        txtQty = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        comUnit = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        txtRs = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtreceive = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtPrdRate = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtVhlNo = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtRoyaltyNo = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtRolMT = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txtDriverName = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtRoyRate = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtRoyRs = new javax.swing.JTextField();
        butCreate = new javax.swing.JButton();
        butClose = new javax.swing.JButton();
        butClear = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        txtAmount = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        jMenuItem3 = new javax.swing.JMenuItem();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Daily Register Entry");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 16));
        jLabel1.setText("Daily Cash Register Entry");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Select Register Type"));

        jLabel7.setText("Challan No:");

        txtChaNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtChaNoActionPerformed(evt);
            }
        });
        txtChaNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtChaNoKeyReleased(evt);
            }
        });

        jLabel8.setText("Select Date:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(dataCho, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(txtChaNo, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(525, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dataCho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(txtChaNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Customer"));

        jLabel4.setText("Customer Name:");

        txtCusName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCusNameActionPerformed(evt);
            }
        });
        txtCusName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCusNameKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCusNameKeyReleased(evt);
            }
        });

        jLabel6.setText("Unloading Place:");

        txtUnloadPla.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUnloadPlaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUnloadPlaKeyReleased(evt);
            }
        });

        jLabel22.setText("Royalty Name:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtUnloadPla, javax.swing.GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
                    .addComponent(txtCusName, javax.swing.GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
                    .addComponent(comRoyaltyName, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(491, 491, 491))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtCusName, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtUnloadPla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comRoyaltyName, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Product Description"));

        jLabel9.setText("Select Product:");

        comPrdName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comPrdNameActionPerformed(evt);
            }
        });

        jLabel10.setText("Quantity:");

        txtQty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtQtyFocusLost(evt);
            }
        });
        txtQty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtQtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtQtyKeyReleased(evt);
            }
        });

        jLabel13.setText("Units:");

        jLabel16.setText("Rs.");

        txtRs.setEnabled(false);
        txtRs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRsKeyReleased(evt);
            }
        });

        jLabel17.setText("Received By:");

        jLabel19.setText("Product Rate");

        txtPrdRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrdRateActionPerformed(evt);
            }
        });
        txtPrdRate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPrdRateFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addGap(18, 18, 18)
                .addComponent(comPrdName, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtQty, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel13)
                .addGap(18, 18, 18)
                .addComponent(comUnit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtPrdRate, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtRs, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtreceive, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(67, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(comPrdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(txtQty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(comUnit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(txtPrdRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(txtRs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(txtreceive, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Vehicle Number"));

        jLabel12.setText("Vehicle Number:");

        txtVhlNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtVhlNoKeyReleased(evt);
            }
        });

        jLabel11.setText("Royalty Number:");

        txtRoyaltyNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRoyaltyNoKeyReleased(evt);
            }
        });

        jLabel14.setText("M.T:");

        txtRolMT.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtRolMTFocusLost(evt);
            }
        });
        txtRolMT.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRolMTKeyReleased(evt);
            }
        });

        jLabel15.setText("Driver Name:");

        jLabel18.setText("Rate:");

        txtRoyRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRoyRateActionPerformed(evt);
            }
        });
        txtRoyRate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtRoyRateFocusLost(evt);
            }
        });
        txtRoyRate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRoyRateKeyReleased(evt);
            }
        });

        jLabel21.setText("Royalty Amount:");

        txtRoyRs.setEnabled(false);
        txtRoyRs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRoyRsKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtVhlNo, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDriverName, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtRoyaltyNo, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtRolMT, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel18)
                .addGap(2, 2, 2)
                .addComponent(txtRoyRate, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtRoyRs, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(231, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtVhlNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(txtDriverName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(txtRoyaltyNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(txtRolMT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(txtRoyRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(txtRoyRs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        butCreate.setText("Create");
        butCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butCreateActionPerformed(evt);
            }
        });

        butClose.setText("Close");
        butClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butCloseActionPerformed(evt);
            }
        });

        butClear.setText("Clear");
        butClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butClearActionPerformed(evt);
            }
        });

        jLabel20.setText("Total Amount:");

        txtAmount.setFont(new java.awt.Font("Tahoma", 0, 18));
        txtAmount.setEnabled(false);
        txtAmount.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtAmountFocusGained(evt);
            }
        });

        jMenu1.setText("File");
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        jMenuItem1.setText("Create");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Clear");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);
        jMenu1.add(jSeparator1);

        jMenuItem3.setText("Close");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(429, 429, 429)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel20)
                                .addGap(18, 18, 18)
                                .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(228, 228, 228)
                                .addComponent(butCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(butClear, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(butClose, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(30, 30, 30)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(butCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(butClear, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(butClose, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void butCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butCloseActionPerformed
        // TODO add your handling code here:
        //this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_butCloseActionPerformed

    private void butClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butClearActionPerformed
        // TODO add your handling code here:
        txtChaNo.setText("");
        txtCusName.setText("");
        txtQty.setText("");
        txtRoyaltyNo.setText("");
        txtUnloadPla.setText("");
        txtVhlNo.setText("");
    }//GEN-LAST:event_butClearActionPerformed

    private void butCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butCreateActionPerformed
 //       java.sql.Date date;
        java.util.Date cashRegisterDate;

        cashChallanNo = txtChaNo.getText();
        productNameID = products.get(comPrdName.getSelectedIndex()).getProductNameId();
        royaltyHolderCusID = customers.get(comRoyaltyName.getSelectedIndex()).getCustomerID();
        productUnitId = productUnits.get(comUnit.getSelectedIndex()).getPrdUnitId();
        cashReceiveBy = txtreceive.getText();
        cashVehicleNo = txtVhlNo.getText();
        cashDriverName = txtDriverName.getText();
        cashReceiveBy = txtreceive.getText();

        if(txtCusName.getText().length()>=0){
            cashCusName = txtCusName.getText();
        }else{
            JOptionPane.showMessageDialog(this, "Please Enter Customer Name", "Invalid Input", JOptionPane.WARNING_MESSAGE);
            return;
        }
        //Qty code
        if (txtQty.getText().length() == 0) {
            JOptionPane.showMessageDialog(this, "Please Enter Product Quantity", "Invalid Input", JOptionPane.WARNING_MESSAGE);
            return;
        } else {
            try {
                cashTranQty = Double.parseDouble(txtQty.getText());
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Invalid Quantity Value", "Invalid Input", JOptionPane.WARNING_MESSAGE);
                return;
            }
        }
        if(txtPrdRate.getText().length()>1){
            productRate = Double.parseDouble(txtPrdRate.getText());
        }else{
            JOptionPane.showMessageDialog(this, "Invalid Quantity Value", "Invalid Input", JOptionPane.WARNING_MESSAGE);
            return;
        }
        //Date Code
        if (dataCho.getSelectedDate() != null) {
            cashRegisterDate = dataCho.getSelectedDate().getTime();
            cashTranDate = new java.sql.Date(cashRegisterDate.getYear(), cashRegisterDate.getMonth(), cashRegisterDate.getDate());
            System.out.println(cashTranDate);
        } else {
            JOptionPane.showMessageDialog(this, "Please Enter Date", "Invalid Input", JOptionPane.WARNING_MESSAGE);
            return;
        }
        //Royalty No code
        if (txtRoyaltyNo.getText().length() == 0) {
            cashTranRoyaltyNo = "";
        } else {
            try {
                cashTranRoyaltyNo = txtRoyaltyNo.getText();
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Invalid Royalty Value", "Invalid Input", JOptionPane.WARNING_MESSAGE);
                return;
            }
        }
        if (txtRoyaltyNo.getText().length() != 0) {
            
        }
        if (txtRolMT.getText().length() == 0) {
            cashTranRoyaltyMT = 0d;
        } else {
            try {
                cashTranRoyaltyMT = Double.parseDouble(txtRolMT.getText());
            } catch (NumberFormatException nfe) {
                JOptionPane.showMessageDialog(this, "Invalid M.T. Value", "Invalid Input", JOptionPane.WARNING_MESSAGE);
                return;
            }
        }

        //Unloading Place Code
        if (txtUnloadPla.getText().length() == 0) {
             cashCusUnloadingPlace = "";
        } else {
            cashCusUnloadingPlace = txtUnloadPla.getText();
            if (cashCusUnloadingPlace.length() >= 60) {
                JOptionPane.showMessageDialog(this, "Too Long Place Name", "Invalid Input", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        //Vehicle Number Code
        if (txtVhlNo.getText().length() == 0) {
            cashVehicleNo = "";
        } else {
            cashVehicleNo = txtVhlNo.getText();
        }
        
        //received by
        if (txtreceive.getText().length() == 0) {
            cashReceiveBy = "";
        } else {
            cashReceiveBy = txtreceive.getText();
        }
        if (txtRoyRate.getText().length() == 0) {

        }else{
            cashRoyaltyRate = Double.parseDouble(txtRoyRate.getText());
        }
        //CustomerMaster cust = customers.get(comRoyaltyName.getSelectedIndex());
        
        System.out.println("Tran Date: "+cashTranDate);
        System.out.println("Chalan No: "+cashChallanNo);
        System.out.println("Customer Name: "+cashCusName);
        System.out.println("Uploading Place "+cashCusUnloadingPlace);
        System.out.println("product ID "+productNameID);
        System.out.println("Product Qty "+cashTranQty);
        System.out.println("Product UnitId: "+productUnitId);
        System.out.println("Product Rate: "+productRate);
        System.out.println("Product MT: "+cashTranRoyaltyMT);
        System.out.println("Vehicel Nao: "+cashVehicleNo);
        System.out.println("Driver Name: "+cashDriverName);
        System.out.println("Royalty No: "+cashTranRoyaltyNo);
        System.out.println("royalty Rate" +cashRoyaltyRate);
        System.out.println("Royalty Hoder Name "+royaltyHolderCusID);
        System.out.println("Cash Receive By: "+cashReceiveBy);


        CashRegisterMaster cashRegister = new CashRegisterMaster(
                cashTranDate,
                cashChallanNo,
                cashCusName,
                cashCusUnloadingPlace,
                productNameID,
                cashTranQty,
                productUnitId,
                productRate,
                cashReceiveBy,
                cashTranRoyaltyMT,
                cashVehicleNo,
                cashDriverName,
                cashTranRoyaltyNo,
                cashRoyaltyRate,
                royaltyHolderCusID
                );
        CashRegisterMasterDAO cashRegisterDAO= new CashRegisterMasterDAO(cmc);
        cashRegisterDAO.AddNewCashRegisterInfo(cashRegister);

        
        //Cash & Challan Code
//        if (comRegType.getSelectedIndex() == 0) { // if create Chllan is selected
//            //check challan number
//            if (txtChaNo.getText().length() == 0) {
//                JOptionPane.showMessageDialog(this, "Please Enter Challan No", "Invalid Input", JOptionPane.WARNING_MESSAGE);
//                return;
//            } else {
//                try {
//                    chlNo = Integer.parseInt(txtChaNo.getText());
//                } catch (NumberFormatException e) {
//                    JOptionPane.showMessageDialog(this, "Invalid Chalaln No", "Invalid Input", JOptionPane.WARNING_MESSAGE);
//                    return;
//                }
//            }
//            //chk customer Name
//
//        }

    }//GEN-LAST:event_butCreateActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        //txtCusName.setEnabled(false);
    }//GEN-LAST:event_formWindowOpened

    private void txtChaNoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtChaNoKeyReleased
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtChaNo.getText().length() >= 1) {
            if (k >= 47 && k <= 57) {
            } else {
                txtChaNo.setText(txtChaNo.getText().substring(0, txtChaNo.getText().length() - 1));
            }
            if (txtChaNo.getText().length() == 11) {
                txtChaNo.setText(txtChaNo.getText().substring(0, txtChaNo.getText().length() - 1));
            }
            if (txtChaNo.getText().length() >= 11) {
                txtChaNo.setText(txtChaNo.getText().substring(0, 10));
            }
        }


    }//GEN-LAST:event_txtChaNoKeyReleased

    private void txtQtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQtyKeyReleased
        // TODO add your handling code here:
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtQty.getText().length() >= 1) {
            if (k >= 47 && k <= 57 || k == '.') {
            } else {
                txtQty.setText(txtQty.getText().substring(0, txtQty.getText().length() - 1));
            }
            if (txtQty.getText().length() == 11) {
                txtQty.setText(txtQty.getText().substring(0, txtQty.getText().length() - 1));
            }
            if (txtQty.getText().length() >= 11) {
                txtQty.setText(txtQty.getText().substring(0, 10));
            }
        }


    }//GEN-LAST:event_txtQtyKeyReleased

    private void txtQtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQtyKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtyKeyPressed

    private void txtVhlNoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVhlNoKeyReleased
        // TODO add your handling code here:
        if (txtVhlNo.getText().length() >= 25) {
            txtVhlNo.setText(txtVhlNo.getText().substring(0, 25));
        }
    }//GEN-LAST:event_txtVhlNoKeyReleased

    private void txtRoyaltyNoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRoyaltyNoKeyReleased
        // TODO add your handling code here:
        if (txtRoyaltyNo.getText().length() >= 9) {
            txtRoyaltyNo.setText(txtRoyaltyNo.getText().substring(0, 9));
        }
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtRoyaltyNo.getText().length() >= 1) {
            if (k >= 47 && k <= 57 || k == '.') {
            } else {
                txtRoyaltyNo.setText(txtRoyaltyNo.getText().substring(0, txtRoyaltyNo.getText().length() - 1));
            }
            if (txtRoyaltyNo.getText().length() == 11) {
                txtRoyaltyNo.setText(txtRoyaltyNo.getText().substring(0, txtRoyaltyNo.getText().length() - 1));
            }
            if (txtRoyaltyNo.getText().length() >= 11) {
                txtRoyaltyNo.setText(txtRoyaltyNo.getText().substring(0, 10));
            }
        }

    }//GEN-LAST:event_txtRoyaltyNoKeyReleased

    private void txtRolMTKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRolMTKeyReleased
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtRolMT.getText().length() >= 1) {
            if (k >= 47 && k <= 57 || k == '.') {
            } else {
                txtRolMT.setText(txtRolMT.getText().substring(0, txtRolMT.getText().length() - 1));
            }
            if (txtRolMT.getText().length() == 7) {
                txtRolMT.setText(txtRolMT.getText().substring(0, txtRolMT.getText().length() - 1));
            }
            if (txtRolMT.getText().length() >= 7) {
                txtRolMT.setText(txtRolMT.getText().substring(0, 7));
            }
        }

    }//GEN-LAST:event_txtRolMTKeyReleased

    private void txtRsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRsKeyReleased
        // TODO add your handling code here:
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtRs.getText().length() >= 1) {
            if (k >= 47 && k <= 57 || k == '.') {
            } else {
                txtRs.setText(txtRs.getText().substring(0, txtRs.getText().length() - 1));
            }
            if (txtRs.getText().length() == 7) {
                txtRs.setText(txtRs.getText().substring(0, txtRs.getText().length() - 1));
            }
            if (txtRs.getText().length() >= 7) {
                txtRs.setText(txtRs.getText().substring(0, 7));
            }
        }
}//GEN-LAST:event_txtRsKeyReleased

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowActivated

    private void comPrdNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comPrdNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comPrdNameActionPerformed

    private void txtRoyRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRoyRateActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_txtRoyRateActionPerformed

    private void txtPrdRateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPrdRateFocusLost
        // TODO add your handling code here:\\\
        float qty;
        float price;
        if (txtQty.getText().length() <= 0) {
            qty = 0f;
        } else {
            qty = Float.parseFloat(txtQty.getText());
        }
        if (txtPrdRate.getText().length() <= 0) {
            price = 0l;
        } else {
            price = Float.parseFloat(txtPrdRate.getText());
        }
        System.out.println(qty * price);
        txtRs.setText("" + qty * price);
        txtAmount.setText("" + qty * price);
        txtRoyRateFocusLost(evt);
}//GEN-LAST:event_txtPrdRateFocusLost

    private void txtQtyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtQtyFocusLost
        // TODO add your handling code here:
        txtPrdRateFocusLost(evt);
    }//GEN-LAST:event_txtQtyFocusLost

    private void txtRoyRateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtRoyRateFocusLost
        // TODO add your handling code here:
        float qty;
        float price;
        if (txtRolMT.getText().length() <= 0) {
            qty = 0f;
        } else {
            qty = Float.parseFloat(txtRolMT.getText());
        }
        if (txtRoyRate.getText().length() <= 0) {
            price = 0f;
        } else {
            price = Float.parseFloat(txtRoyRate.getText());
        }
        System.out.println(qty * price);
        txtRoyRs.setText("" + qty * price);
        float amt = Float.parseFloat(txtRs.getText()) + Float.parseFloat(txtRoyRs.getText());
        txtAmount.setText("" + amt);
}//GEN-LAST:event_txtRoyRateFocusLost

    private void txtRoyRsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRoyRsKeyReleased
        // TODO add your handling code here:
}//GEN-LAST:event_txtRoyRsKeyReleased

    private void txtRolMTFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtRolMTFocusLost
        // TODO add your handling code here:
        txtRoyRateFocusLost(evt);
    }//GEN-LAST:event_txtRolMTFocusLost

    private void txtAmountFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtAmountFocusGained
        // TODO add your handling code here:
        txtAmount.setText("" + (Float.parseFloat(txtRs.getText()) + Float.parseFloat(txtRoyRs.getText())));
}//GEN-LAST:event_txtAmountFocusGained

    private void txtRoyRateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRoyRateKeyReleased
        // TODO add your handling code here:
        char k = evt.getKeyChar();
        System.out.println(k);
        //System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("You Press Back Space");
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_SHIFT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_ALT) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            return;
        }
        if (evt.getKeyCode() == KeyEvent.VK_HOME) {
            return;
        }
        if (txtRoyRate.getText().length() >= 1) {
            if (k >= 47 && k <= 57 || k == '.') {
            } else {
                txtRoyRate.setText(txtRoyRate.getText().substring(0, txtRoyRate.getText().length() - 1));
            }
            if (txtRoyRate.getText().length() == 11) {
                txtRoyRate.setText(txtRoyRate.getText().substring(0, txtRoyRate.getText().length() - 1));
            }
            if (txtRoyRate.getText().length() >= 11) {
                txtRoyRate.setText(txtRoyRate.getText().substring(0, 10));
            }
        }
    }//GEN-LAST:event_txtRoyRateKeyReleased

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        butCreateActionPerformed(evt);

    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        butClearActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void txtUnloadPlaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUnloadPlaKeyReleased
        // TODO add your handling code here:
        char k = evt.getKeyChar();

        if (txtUnloadPla.getText().length() >= 1) {
            if (k >= 47 && k <= 57) {
                txtUnloadPla.setText(txtUnloadPla.getText().substring(0, txtUnloadPla.getText().length() - 1));
            } else {
            }

        }
        if (txtUnloadPla.getText().length() >= 40) {
            txtUnloadPla.setText(txtUnloadPla.getText().substring(0, txtUnloadPla.getText().length() - 1));
        }
}//GEN-LAST:event_txtUnloadPlaKeyReleased

    private void txtUnloadPlaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUnloadPlaKeyPressed
        // TODO add your handling code here:
}//GEN-LAST:event_txtUnloadPlaKeyPressed

    private void txtCusNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCusNameKeyReleased

        char k = evt.getKeyChar();

        if (txtCusName.getText().length() >= 1) {
            if (k >= 47 && k <= 57) {
                txtCusName.setText(txtCusName.getText().substring(0, txtCusName.getText().length() - 1));
            } else {
            }
            if (txtCusName.getText().length() >= 50) {
                txtCusName.setText(txtCusName.getText().substring(0, txtCusName.getText().length() - 1));
            }
        }
}//GEN-LAST:event_txtCusNameKeyReleased

    private void txtCusNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCusNameKeyPressed

}//GEN-LAST:event_txtCusNameKeyPressed

    private void txtChaNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtChaNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtChaNoActionPerformed

    private void txtCusNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCusNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCusNameActionPerformed

    private void txtPrdRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrdRateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrdRateActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new CashRegister_backup().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton butClear;
    private javax.swing.JButton butClose;
    private javax.swing.JButton butCreate;
    private javax.swing.JComboBox comPrdName;
    private javax.swing.JComboBox comRoyaltyName;
    private javax.swing.JComboBox comUnit;
    private datechooser.beans.DateChooserCombo dataCho;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txtAmount;
    private javax.swing.JTextField txtChaNo;
    private javax.swing.JTextField txtCusName;
    private javax.swing.JTextField txtDriverName;
    private javax.swing.JTextField txtPrdRate;
    private javax.swing.JTextField txtQty;
    private javax.swing.JTextField txtRolMT;
    private javax.swing.JTextField txtRoyRate;
    private javax.swing.JTextField txtRoyRs;
    private javax.swing.JTextField txtRoyaltyNo;
    private javax.swing.JTextField txtRs;
    private javax.swing.JTextField txtUnloadPla;
    private javax.swing.JTextField txtVhlNo;
    private javax.swing.JTextField txtreceive;
    // End of variables declaration//GEN-END:variables
}
