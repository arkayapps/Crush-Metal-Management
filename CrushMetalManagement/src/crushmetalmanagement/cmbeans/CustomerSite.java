/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package crushmetalmanagement.cmbeans;

/**
 *
 * @author Acer
 */
public class CustomerSite {
    private int siteID;
    private String siteName;
    private String siteDescription;
    private int customerID;

    public CustomerSite(int siteID, String siteName, String siteDescription, int customerID) {
        this.siteID = siteID;
        this.siteName = siteName;
        this.siteDescription = siteDescription;
        this.customerID = customerID;
    }

    public CustomerSite() {
    }
    
 public CustomerSite(String siteName, String siteDescription, int customerID) {
        this.siteName = siteName;
        this.siteDescription = siteDescription;
        this.customerID = customerID;
    }
    
    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getSiteDescription() {
        return siteDescription;
    }

    public void setSiteDescription(String siteDescription) {
        this.siteDescription = siteDescription;
    }

    public int getSiteID() {
        return siteID;
    }

    public void setSiteID(int siteID) {
        this.siteID = siteID;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }
    public CustomerSite(int CusID) {
        this.customerID = CusID;
    }
        @Override
    public boolean equals(Object obj) {
        if(obj instanceof CustomerSite && ((CustomerSite)obj).getSiteID() == this.siteID)
            return true;

        return false;
    }

    @Override
    public int hashCode() {
        return this.siteID;
    }

    @Override
    public String toString() {
        return "Site ID :"+this.siteID +" Site Name: "+siteName;
    }

}
