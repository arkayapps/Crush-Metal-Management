/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package crushmetalmanagement.cmbeans;

/**
 *
 * @author Acer
 */
public class ProductUnit {
    private int PrdUnitId;
    private String PrdUnitName;

    public ProductUnit(int PrdUnitId, String PrdUnitName) {
        this.PrdUnitId = PrdUnitId;
        this.PrdUnitName = PrdUnitName;
    }

    public int getPrdUnitId() {
        return PrdUnitId;
    }

    public void setPrdUnitId(int PrdUnitId) {
        this.PrdUnitId = PrdUnitId;
    }

    public String getPrdUnitName() {
        return PrdUnitName;
    }

    public void setPrdUnitName(String PrdUnitName) {
        this.PrdUnitName = PrdUnitName;
    }
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ProductUnit && ((ProductUnit)obj).getPrdUnitId() == this.PrdUnitId)
            return true;

        return false;
    }

    @Override
    public int hashCode() {
        return this.PrdUnitId;
    }

    @Override
    public String toString() {
        return "Product Unit ID :"+this.PrdUnitId +" Product Unit Name: "+PrdUnitName;
    }
    
}
