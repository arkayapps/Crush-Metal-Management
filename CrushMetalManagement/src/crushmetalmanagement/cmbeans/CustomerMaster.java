/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package crushmetalmanagement.cmbeans;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Acer
 */
public class CustomerMaster {
    private int customerID;
    private String customerName;
    private String customerAddress;
    private String customerCity;
    private String customerState;
    private String customerZip;
    private String customerMobile;
    private String customerPhone;
    private boolean isVisible;
    private List<CustomerSite> customerSites;


    public CustomerMaster( String customerName, String customerAddress, String customerCity, String customerState, String customerZip, String customerMobile, String customerPhone, boolean isVisible) {
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.customerCity = customerCity;
        this.customerState = customerState;
        this.customerZip = customerZip;
        this.customerMobile = customerMobile;
        this.customerPhone = customerPhone;
        this.isVisible = isVisible;
        this.customerSites = new ArrayList<CustomerSite>();
    }

    public CustomerMaster(int customerID, String customerName, String customerAddress, String customerCity, String customerState, String customerZip, String customerMobile, String customerPhone, boolean isVisible) {
        this.customerID = customerID;
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.customerCity = customerCity;
        this.customerState = customerState;
        this.customerZip = customerZip;
        this.customerMobile = customerMobile;
        this.customerPhone = customerPhone;
        this.isVisible = isVisible;
        this.customerSites = new ArrayList<CustomerSite>();
    }

    

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerState() {
        return customerState;
    }

    public void setCustomerState(String customerState) {
        this.customerState = customerState;
    }

    public String getCustomerZip() {
        return customerZip;
    }

    public void setCustomerZip(String customerZip) {
        this.customerZip = customerZip;
    }

    public boolean isIsVisible() {
        return isVisible;
    }

    public void setIsVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public CustomerMaster(int customerID) {
        this.customerID = customerID;
    }

    public List<CustomerSite> getCustomerSites() {
        return customerSites;
    }

    public void setCustomerSites(List<CustomerSite> customerSites) {
        this.customerSites = customerSites;
    }
        @Override
    public boolean equals(Object obj) {
        if(obj instanceof CustomerMaster && ((CustomerMaster)obj).getCustomerID() == this.getCustomerID())
            return true;

        return false;
    }

    @Override
    public int hashCode() {
        return this.customerID;
    }

    @Override
    public String toString() {
        return "Customer ID :"+this.customerID +" Customer Name: "+customerName;
    }


}
