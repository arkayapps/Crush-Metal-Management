/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package crushmetalmanagement.cmbeans;

import java.sql.Date;
import sun.security.krb5.internal.ccache.CCacheInputStream;

/**
 *
 * @author Acer
 */
public class CreditRegisterMaster {
    private int creditRegisterId;
    private Date creditTranDate;
    private String creditChallanNo;
    // private int creditCusId;
    private CustomerMaster customer;

    private int creditSiteID;
    private String creditCusUnloadingPlace;
    private int productNameID;
    private Double creditTranQty;
    private int productUnitId;
    private Double productRate;
    private Double creditTranRoyaltyMT;
    private String creditVehicleNo;
    private String creditDriverName;
    private String creditTranRoyaltyNo;
    private Double creditRoyaltyRate;
    private int royaltyHolderCusID;

    public CreditRegisterMaster(int creditRegisterId, Date creditTranDate, String creditChallanNo, CustomerMaster customer, int creditSiteID, String creditCusUnloadingPlace, int productNameID, Double creditTranQty, int productUnitId, Double productRate, Double creditTranRoyaltyMT, String creditVehicleNo, String creditDriverName, String creditTranRoyaltyNo, Double creditRoyaltyRate, int royaltyHolderCusID) {
        this.creditRegisterId = creditRegisterId;
        this.creditTranDate = creditTranDate;
        this.creditChallanNo = creditChallanNo;
        //this.creditCusId = creditCusId;
        this.customer = customer;
        this.creditSiteID = creditSiteID;
        this.creditCusUnloadingPlace = creditCusUnloadingPlace;
        this.productNameID = productNameID;
        this.creditTranQty = creditTranQty;
        this.productUnitId = productUnitId;
        this.productRate = productRate;
        this.creditTranRoyaltyMT = creditTranRoyaltyMT;
        this.creditVehicleNo = creditVehicleNo;
        this.creditDriverName = creditDriverName;
        this.creditTranRoyaltyNo = creditTranRoyaltyNo;
        this.creditRoyaltyRate = creditRoyaltyRate;
        this.royaltyHolderCusID = royaltyHolderCusID;
    }
    public CreditRegisterMaster(Date creditTranDate, String creditChallanNo, CustomerMaster customer, int creditSiteID, String creditCusUnloadingPlace, int productNameID, Double creditTranQty, int productUnitId, Double productRate, Double creditTranRoyaltyMT, String creditVehicleNo, String creditDriverName, String creditTranRoyaltyNo, Double creditRoyaltyRate, int royaltyHolderCusID) {
        this.creditTranDate = creditTranDate;
        this.creditChallanNo = creditChallanNo;
        //this.creditCusId = creditCusId;
        this.customer = customer;
        this.creditSiteID= creditSiteID;
        this.creditCusUnloadingPlace = creditCusUnloadingPlace;
        this.productNameID = productNameID;
        this.creditTranQty = creditTranQty;
        this.productUnitId = productUnitId;
        this.productRate = productRate;
        this.creditTranRoyaltyMT = creditTranRoyaltyMT;
        this.creditVehicleNo = creditVehicleNo;
        this.creditDriverName = creditDriverName;
        this.creditTranRoyaltyNo = creditTranRoyaltyNo;
        this.creditRoyaltyRate = creditRoyaltyRate;
        this.royaltyHolderCusID = royaltyHolderCusID;
    }
    public String getCreditChallanNo() {
        return creditChallanNo;
    }

    public void setCreditChallanNo(String creditChallanNo) {
        this.creditChallanNo = creditChallanNo;
    }

//    public int getCreditCusId() {
//        return creditCusId;
//    }
//
//    public void setCreditCusId(int creditCusId) {
//        this.creditCusId = creditCusId;
//    }


    public String getCreditCusUnloadingPlace() {
        return creditCusUnloadingPlace;
    }

    public void setCreditCusUnloadingPlace(String creditCusUnloadingPlace) {
        this.creditCusUnloadingPlace = creditCusUnloadingPlace;
    }

    public String getCreditDriverName() {
        return creditDriverName;
    }

    public void setCreditDriverName(String creditDriverName) {
        this.creditDriverName = creditDriverName;
    }

    public int getCreditRegisterId() {
        return creditRegisterId;
    }

    public void setCreditRegisterId(int creditRegisterId) {
        this.creditRegisterId = creditRegisterId;
    }

    public Double getCreditRoyaltyRate() {
        return creditRoyaltyRate;
    }

    public void setCreditRoyaltyRate(Double creditRoyaltyRate) {
        this.creditRoyaltyRate = creditRoyaltyRate;
    }

    public Date getCreditTranDate() {
        return creditTranDate;
    }

    public void setCreditTranDate(Date creditTranDate) {
        this.creditTranDate = creditTranDate;
    }

    public Double getCreditTranQty() {
        return creditTranQty;
    }

    public void setCreditTranQty(Double creditTranQty) {
        this.creditTranQty = creditTranQty;
    }

    public Double getCreditTranRoyaltyMT() {
        return creditTranRoyaltyMT;
    }

    public void setCreditTranRoyaltyMT(Double creditTranRoyaltyMT) {
        this.creditTranRoyaltyMT = creditTranRoyaltyMT;
    }

    public String getCreditTranRoyaltyNo() {
        return creditTranRoyaltyNo;
    }

    public void setCreditTranRoyaltyNo(String creditTranRoyaltyNo) {
        this.creditTranRoyaltyNo = creditTranRoyaltyNo;
    }

    public String getCreditVehicleNo() {
        return creditVehicleNo;
    }

    public void setCreditVehicleNo(String creditVehicleNo) {
        this.creditVehicleNo = creditVehicleNo;
    }

    public int getProductNameID() {
        return productNameID;
    }

    public void setProductNameID(int productNameID) {
        this.productNameID = productNameID;
    }

    public Double getProductRate() {
        return productRate;
    }

    public void setProductRate(Double productRate) {
        this.productRate = productRate;
    }

    public int getProductUnitId() {
        return productUnitId;
    }

    public void setProductUnitId(int productUnitId) {
        this.productUnitId = productUnitId;
    }

    public int getRoyaltyHolderCusID() {
        return royaltyHolderCusID;
    }

    public void setRoyaltyHolderCusID(int royaltyHolderCusID) {
        this.royaltyHolderCusID = royaltyHolderCusID;
    }

    public int getCreditSiteID() {
        return creditSiteID;
    }

    public void setCreditSiteID(int creditSiteID) {
        this.creditSiteID = creditSiteID;
    }

    public CustomerMaster getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerMaster customer) {
        this.customer = customer;
    }

           @Override
    public boolean equals(Object obj) {
        if(obj instanceof CreditRegisterMaster && ((CreditRegisterMaster)obj).getCreditRegisterId() == this.getCreditRegisterId())
            return true;

        return false;
    }

    @Override
    public int hashCode() {
        return this.getCreditRegisterId();
    }

    @Override
    public String toString() {
        return "Credit Register Id() :"+this.getCreditRegisterId() ;
    }
}
