/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package crushmetalmanagement.cmbeans;

/**
 *
 * @author Acer
 */
public class Product {
    private int ProductNameId;
    private String productName;

    public Product(int ProductNameId, String productName) {
        this.ProductNameId = ProductNameId;
        this.productName = productName;
    }

    public int getProductNameId() {
        return ProductNameId;
    }

    public void setProductNameId(int ProductNameId) {
        this.ProductNameId = ProductNameId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Product && ((Product)obj).getProductNameId() == this.ProductNameId)
            return true;

        return false;
    }

    @Override
    public int hashCode() {
        return this.ProductNameId;
    }

    @Override
    public String toString() {
        return "Product ID :"+this.ProductNameId +" Product Name: "+productName ;
    }
    


       
}
