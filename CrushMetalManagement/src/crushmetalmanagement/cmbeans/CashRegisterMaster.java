/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package crushmetalmanagement.cmbeans;

import java.sql.Date;


/**
 *
 * @author Acer
 */
public class CashRegisterMaster {
    private int cashRegisterId;
    private Date cashTranDate;
    private String cashChallanNo;
    private String cashCusName;
    private String cashCusUnloadingPlace;
    private int productNameID;
    private Double cashTranQty;
    private int productUnitId;
    private Double productRate;
    private String cashReceiveBy;
    private Double cashTranRoyaltyMT;
    private String cashVehicleNo;
    private String cashDriverName;
    private String cashTranRoyaltyNo;
    private Double cashRoyaltyRate;
    private int royaltyHolderCusID;

    public CashRegisterMaster(int cashRegisterId, Date cashTranDate, String cashChallanNo, String cashCusName, String cashCusUnloadingPlace, int productNameID, Double cashTranQty, int productUnitId, Double productRate, String cashReceiveBy, Double cashTranRoyaltyMT, String cashVehicleNo, String cashDriverName, String cashTranRoyaltyNo, Double cashRoyaltyRate, int royaltyHolderCusID) {
        this.cashRegisterId = cashRegisterId;
        this.cashTranDate = cashTranDate;
        this.cashChallanNo = cashChallanNo;
        this.cashCusName = cashCusName;
        this.cashCusUnloadingPlace = cashCusUnloadingPlace;
        this.productNameID = productNameID;
        this.cashTranQty = cashTranQty;
        this.productUnitId = productUnitId;
        this.productRate = productRate;
        this.cashReceiveBy = cashReceiveBy;
        this.cashTranRoyaltyMT = cashTranRoyaltyMT;
        this.cashVehicleNo = cashVehicleNo;
        this.cashDriverName = cashDriverName;
        this.cashTranRoyaltyNo = cashTranRoyaltyNo;
        this.cashRoyaltyRate = cashRoyaltyRate;
        this.royaltyHolderCusID = royaltyHolderCusID;
    }

    public CashRegisterMaster(Date cashTranDate, String cashChallanNo, String cashCusName, String cashCusUnloadingPlace, int productNameID, Double cashTranQty, int productUnitId, Double productRate, String cashReceiveBy, Double cashTranRoyaltyMT, String cashVehicleNo, String cashDriverName, String cashTranRoyaltyNo, Double cashRoyaltyRate, int royaltyHolderCusID) {
        this.cashTranDate = cashTranDate;
        this.cashChallanNo = cashChallanNo;
        this.cashCusName = cashCusName;
        this.cashCusUnloadingPlace = cashCusUnloadingPlace;
        this.productNameID = productNameID;
        this.cashTranQty = cashTranQty;
        this.productUnitId = productUnitId;
        this.productRate = productRate;
        this.cashReceiveBy = cashReceiveBy;
        this.cashTranRoyaltyMT = cashTranRoyaltyMT;
        this.cashVehicleNo = cashVehicleNo;
        this.cashDriverName = cashDriverName;
        this.cashTranRoyaltyNo = cashTranRoyaltyNo;
        this.cashRoyaltyRate = cashRoyaltyRate;
        this.royaltyHolderCusID = royaltyHolderCusID;
    }

    public String getCashChallanNo() {
        return cashChallanNo;
    }

    public void setCashChallanNo(String cashChallanNo) {
        this.cashChallanNo = cashChallanNo;
    }

    public String getCashCusName() {
        return cashCusName;
    }

    public void setCashCusName(String cashCusName) {
        this.cashCusName = cashCusName;
    }

    public String getCashCusUnloadingPlace() {
        return cashCusUnloadingPlace;
    }

    public void setCashCusUnloadingPlace(String cashCusUnloadingPlace) {
        this.cashCusUnloadingPlace = cashCusUnloadingPlace;
    }

    public String getCashDriverName() {
        return cashDriverName;
    }

    public void setCashDriverName(String cashDriverName) {
        this.cashDriverName = cashDriverName;
    }

    public String getCashReceiveBy() {
        return cashReceiveBy;
    }

    public void setCashReceiveBy(String cashReceiveBy) {
        this.cashReceiveBy = cashReceiveBy;
    }

    public int getCashRegisterId() {
        return cashRegisterId;
    }

    public void setCashRegisterId(int cashRegisterId) {
        this.cashRegisterId = cashRegisterId;
    }

    public Double getCashRoyaltyRate() {
        return cashRoyaltyRate;
    }

    public void setCashRoyaltyRate(Double cashRoyaltyRate) {
        this.cashRoyaltyRate = cashRoyaltyRate;
    }

    public Date getCashTranDate() {
        return cashTranDate;
    }

    public void setCashTranDate(Date cashTranDate) {
        this.cashTranDate = cashTranDate;
    }

    public Double getCashTranQty() {
        return cashTranQty;
    }

    public void setCashTranQty(Double cashTranQty) {
        this.cashTranQty = cashTranQty;
    }

    public Double getCashTranRoyaltyMT() {
        return cashTranRoyaltyMT;
    }

    public void setCashTranRoyaltyMT(Double cashTranRoyaltyMT) {
        this.cashTranRoyaltyMT = cashTranRoyaltyMT;
    }

    public String getCashTranRoyaltyNo() {
        return cashTranRoyaltyNo;
    }

    public void setCashTranRoyaltyNo(String cashTranRoyaltyNo) {
        this.cashTranRoyaltyNo = cashTranRoyaltyNo;
    }

    public String getCashVehicleNo() {
        return cashVehicleNo;
    }

    public void setCashVehicleNo(String cashVehicleNo) {
        this.cashVehicleNo = cashVehicleNo;
    }

    public int getProductNameID() {
        return productNameID;
    }

    public void setProductNameID(int productNameID) {
        this.productNameID = productNameID;
    }

    public Double getProductRate() {
        return productRate;
    }

    public void setProductRate(Double productRate) {
        this.productRate = productRate;
    }

    public int getProductUnitId() {
        return productUnitId;
    }

    public void setProductUnitId(int productUnitId) {
        this.productUnitId = productUnitId;
    }

    public int getRoyaltyHolderCusID() {
        return royaltyHolderCusID;
    }

    public void setRoyaltyHolderCusID(int royaltyHolderCusID) {
        this.royaltyHolderCusID = royaltyHolderCusID;
    }
   
           @Override
    public boolean equals(Object obj) {
        if(obj instanceof CashRegisterMaster && ((CashRegisterMaster)obj).getCashRegisterId() == this.getCashRegisterId())
            return true;

        return false;
    }

    @Override
    public int hashCode() {
        return this.getCashRegisterId();
    }

    @Override
    public String toString() {
        return "Cash Register Id() :"+this.getCashRegisterId();
    }
}
