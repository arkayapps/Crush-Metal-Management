package crushmetalmanagement.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import crushmetalmanagement.cmbeans.CashRegisterMaster;
import crushmetalmanagement.cmbeans.CreditRegisterMaster;



public class CashRegisterMasterDAO {
	private CrushMetalConnection cmc;

        public CashRegisterMasterDAO(CrushMetalConnection cmc){
            this.cmc = cmc;
        }

	public boolean AddNewCashRegisterInfo(CashRegisterMaster cashRegisterMaster) {
		 try {
	            PreparedStatement prep = cmc.getConnection().prepareStatement("insert into " + TableFiled.CASHREGISTER_TABLE + " values (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
	            // prep.setInt(0, null); Auto increment
	            prep.setLong(1, cashRegisterMaster.getCashTranDate().getTime());
	            prep.setString(2, cashRegisterMaster.getCashChallanNo());
	            prep.setString(3, cashRegisterMaster.getCashCusName());
	            prep.setString(4, cashRegisterMaster.getCashCusUnloadingPlace());
	            prep.setInt(5, cashRegisterMaster.getProductNameID());
	            prep.setDouble(6, cashRegisterMaster.getCashTranQty());
	            prep.setInt(7, cashRegisterMaster.getProductUnitId());
	            prep.setDouble(8, cashRegisterMaster.getProductRate());
	            prep.setString(9, cashRegisterMaster.getCashReceiveBy());
	            prep.setDouble(10, cashRegisterMaster.getCashTranRoyaltyMT());
	            prep.setString(11, cashRegisterMaster.getCashVehicleNo());
	            prep.setString(12, cashRegisterMaster.getCashDriverName());
	            prep.setString(13, cashRegisterMaster.getCashTranRoyaltyNo());
	            prep.setDouble(14, cashRegisterMaster.getCashRoyaltyRate());
	            prep.setInt(15, cashRegisterMaster.getRoyaltyHolderCusID());
	            prep.addBatch();
	            cmc.getConnection().setAutoCommit(false);
	            prep.executeBatch();
	            cmc.getConnection().setAutoCommit(true);

	        } catch (Exception e) {
	            System.err.println(e);
	            return false;
	        }
	        return true;
	 }
	 public boolean updateCashRegisterInfo(CashRegisterMaster cashRegisterMaster) {
		 try {
	            PreparedStatement prep = cmc.getConnection().prepareStatement("update " + TableFiled.CASHREGISTER_TABLE +" set "
	            			+ TableFiled.CASH_TRAN_DATE + " = ?,"
	            			+ TableFiled.CASH_CHALLAN_NO + " = ?, "
	            			+ TableFiled.CASH_CUS_NAME + " = ?, "
	            			+ TableFiled.CASH_CUS_UNLOADING_PLACE + " = ?, "
	            			+ TableFiled.CASH_PRODUCTNAME_ID +" = ?, "
	            			+ TableFiled.CASH_TRAN_QTY + " = ?, "
	            			+ TableFiled.CASH_PRODUCTUNIT_ID + " = ?, "
	            			+ TableFiled.CASH_PRODUCT_RATE + " = ?, "
	            			+ TableFiled.CASH_RECEIVEBY + " = ?, "
	            			+ TableFiled.CASH_TRAN_ROYALTY_MT +" = ?, "
	            			+ TableFiled.CASH_VEHICLE_NO + " = ?, "
	            			+ TableFiled.CASH_DRIVER_NAME + " = ?, "
	            			+ TableFiled.CASH_TRAN_ROYALTY_NO + " = ?, "
	            			+ TableFiled.CASH_ROYALTY_RATE + " = ?, "
	            			+ TableFiled.CASH_ROYALTY_HOLDER_CUS_ID + " = ? "
	            			+ " WHERE "
	            			+ TableFiled.CASH_REGISTER_ID +" = ? ");
	            //prep.set(1, null);
	            prep.setLong(1, cashRegisterMaster.getCashTranDate().getTime());
	            prep.setString(2, cashRegisterMaster.getCashChallanNo());
	            prep.setString(3, cashRegisterMaster.getCashCusName());
	            prep.setString(4, cashRegisterMaster.getCashCusUnloadingPlace());
	            prep.setInt(5, cashRegisterMaster.getProductNameID());
	            prep.setDouble(6, cashRegisterMaster.getCashTranQty());
	            prep.setInt(7, cashRegisterMaster.getProductUnitId());
	            prep.setDouble(8, cashRegisterMaster.getProductRate());
	            prep.setString(9, cashRegisterMaster.getCashReceiveBy());
	            prep.setDouble(10, cashRegisterMaster.getCashTranRoyaltyMT());
	            prep.setString(11, cashRegisterMaster.getCashVehicleNo());
	            prep.setString(12, cashRegisterMaster.getCashDriverName());
	            prep.setString(13, cashRegisterMaster.getCashTranRoyaltyNo());
	            prep.setDouble(14, cashRegisterMaster.getCashRoyaltyRate());
	            prep.setInt(15, cashRegisterMaster.getRoyaltyHolderCusID());
	            prep.setInt(16, cashRegisterMaster.getCashRegisterId());
	            //  prep.addBatch();
	            cmc.getConnection().setAutoCommit(false);
	            prep.executeUpdate();
	            cmc.getConnection().setAutoCommit(true);

	            
	        } catch (Exception e) {
	            System.err.println(e);
	            return false;
	        }
	        return true;
	 }
	 public boolean deleteCashRegisterInfo(CrushMetalConnection cmc ,CashRegisterMaster cashRegisterMaster) {
		 try {
	            PreparedStatement prep = cmc.getConnection().prepareStatement("delete from " + TableFiled.CASHREGISTER_TABLE
	            			+ " WHERE "
	            			+ TableFiled.CASH_REGISTER_ID +" = ? ");
	           
	            prep.setInt(1, cashRegisterMaster.getCashRegisterId());
	            //  prep.addBatch();
	            cmc.getConnection().setAutoCommit(false);
	            prep.executeUpdate();
	            cmc.getConnection().setAutoCommit(true);

	            
	        } catch (Exception e) {
	            System.err.println(e);
	            return false;
	        }
	        return true;
	 }

	 public List<CashRegisterMaster> getAllCashRegisterInfo() {
	        List<CashRegisterMaster> cashRegisterMasterList = new ArrayList<CashRegisterMaster>();
	        try {
	            Statement stat = cmc.getConnection().createStatement();
	            ResultSet rs = stat.executeQuery("select * from "+TableFiled.CASHREGISTER_TABLE);
	            CashRegisterMaster cashRegisterMaster;
	            while (rs.next()) {
	                //System.out.println("name = " + rs.getString("customer_name"));
	            	long date=rs.getLong(TableFiled.CASH_TRAN_DATE);
	            	Date newDate=new Date(date);
	            	cashRegisterMaster = new CashRegisterMaster(rs.getInt(TableFiled.CASH_REGISTER_ID), newDate, rs.getString(TableFiled.CASH_CHALLAN_NO), rs.getString(TableFiled.CASH_CUS_NAME), rs.getString(TableFiled.CASH_CUS_UNLOADING_PLACE), rs.getInt(TableFiled.CASH_PRODUCTNAME_ID), rs.getDouble(TableFiled.CASH_TRAN_QTY), rs.getInt(TableFiled.CASH_PRODUCTUNIT_ID), rs.getDouble(TableFiled.CASH_PRODUCT_RATE), rs.getString(TableFiled.CASH_RECEIVEBY), rs.getDouble(TableFiled.CASH_TRAN_ROYALTY_MT),rs.getString(TableFiled.CASH_VEHICLE_NO), rs.getString(TableFiled.CASH_DRIVER_NAME), rs.getString(TableFiled.CASH_TRAN_ROYALTY_NO), rs.getDouble(TableFiled.CASH_ROYALTY_RATE), rs.getInt(TableFiled.CASH_ROYALTY_HOLDER_CUS_ID));
	            	cashRegisterMasterList.add(cashRegisterMaster);
	            }
	            rs.close();
	        } catch (SQLException sql) {
	            System.err.println(sql);
	        }
	        return cashRegisterMasterList;
	    }


}
