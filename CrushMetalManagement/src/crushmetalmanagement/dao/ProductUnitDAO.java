/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package crushmetalmanagement.dao;

import java.util.List;
import crushmetalmanagement.cmbeans.ProductUnit;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
/**
 *
 * @author Acer
 */
public class ProductUnitDAO {
    CrushMetalConnection cmc;
    public ProductUnitDAO(CrushMetalConnection cmc){
        this.cmc = cmc;
    }
 public List<ProductUnit> getAllProductUnit(){
        List<ProductUnit> productUnits = new ArrayList<ProductUnit>();
        try {
            Statement stat = cmc.getConnection().createStatement();
            ResultSet rs = stat.executeQuery("select * from Product_unit;");
            ProductUnit tempProductUnit;
            while(rs.next()){
                tempProductUnit = new ProductUnit(rs.getInt(TableFiled.PRD_UNIT_ID), rs.getString(TableFiled.PRD_UNIT_NAME));
                productUnits.add(tempProductUnit);
            }

        }catch(SQLException sqle){
            System.err.println(sqle);
        }
        return productUnits;
    }
}
