/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package crushmetalmanagement.dao;

import crushmetalmanagement.cmbeans.CustomerMaster;
import crushmetalmanagement.cmbeans.CustomerSite;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Acer
 */
public class CustomeMasterDAO {

    private List<CustomerSite> customerSite;
    private CrushMetalConnection cmc;
    // CrushMetalConnection cmc = new CrushMetalConnection();
    public CustomeMasterDAO(CrushMetalConnection cmc){
        this.cmc = cmc;
        //CustomerSiteDAO customerSite = new CustomerSiteDAO(cmc);
        CustomerSiteDAO custSites = new CustomerSiteDAO(cmc);
        customerSite = custSites.getAllCustomerSites();
        //setSite();
    }
    public int AddNewCustomer(CustomerMaster cutsomerInfo) {
        int cusId =0;
        boolean isAddNewCustomer = false;
        PreparedStatement prep;
        try {
            prep = cmc.getConnection().prepareStatement("insert into customer_master values (NULL, ?, ?, ?, ?, ?, ?, ?, ?)");
            //prep.set(1, null);
            prep.setString(1, cutsomerInfo.getCustomerName());
            prep.setString(2, cutsomerInfo.getCustomerAddress());
            prep.setString(3, cutsomerInfo.getCustomerCity());
            prep.setString(4, cutsomerInfo.getCustomerState());
            prep.setString(5, cutsomerInfo.getCustomerZip());
            prep.setString(6, cutsomerInfo.getCustomerMobile());
            prep.setString(7, cutsomerInfo.getCustomerPhone());
            prep.setBoolean(8, true);
            prep.addBatch();
            cmc.getConnection().setAutoCommit(false);
            isAddNewCustomer = prep.executeBatch().length >= 1 ? true : false;
            cmc.getConnection().setAutoCommit(true);
            prep.close();
        } catch (Exception e) {
            System.err.println(e);
            isAddNewCustomer = false;
            cusId =0;
        }
        if(isAddNewCustomer){
            try{
                Statement stat = cmc.getConnection().createStatement();
                ResultSet rs = stat.executeQuery("SELECT max(customer_id) FROM customer_master;");
                cusId = rs.getInt(1);
                rs.close();
                stat.close();
            }catch(Exception e){
                System.out.println(""+e);
            }
        }
        return cusId;
    }

    public boolean UpdateCustomer(CustomerMaster cutsomerInfo) {
        try {
            PreparedStatement prep = cmc.getConnection().prepareStatement("UPDATE customer_master SET  " + TableFiled.CUSTOMER_NAME + "=?," + TableFiled.CUSTOMER_ADDRESS + "=?," + TableFiled.CUSTOMER_CITY + "=?," + TableFiled.CUSTOMER_STATE + "=?," + TableFiled.CUSTOMER_ZIP + "=?," + TableFiled.CUSTOMER_MOBILE + "=?," + TableFiled.CUSTOMER_PHONE + "=?," + TableFiled.CUSTOMER_VISIBLE + "=? WHERE " + TableFiled.CUSTOMER_ID + "=?;");
            //prep.set(1, null);
            prep.setString(1, cutsomerInfo.getCustomerName());
            prep.setString(2, cutsomerInfo.getCustomerAddress());
            prep.setString(3, cutsomerInfo.getCustomerCity());
            prep.setString(4, cutsomerInfo.getCustomerState());
            prep.setString(5, cutsomerInfo.getCustomerZip());
            prep.setString(6, cutsomerInfo.getCustomerMobile());
            prep.setString(7, cutsomerInfo.getCustomerPhone());
            prep.setBoolean(8, true);
            prep.setInt(9, cutsomerInfo.getCustomerID());

            prep.addBatch();
            cmc.getConnection().setAutoCommit(false);
            prep.executeBatch();
            cmc.getConnection().setAutoCommit(true);


        } catch (Exception e) {
            System.err.println(e);
            return false;
        }
        return true;
    }

    public boolean DeleteCustomer(CustomerMaster customer) {
        try {
            // String s="delete from customer_master,customer_site where "+TableFiled.CUSTOMER_ID+"=?;";
            String s1="delete from customer_site where "+TableFiled.CUSTOMER_ID+"=?;";

            PreparedStatement prep = cmc.getConnection().prepareStatement("delete from customer_master where " + TableFiled.CUSTOMER_ID + "=?;");
            //prep.set(1, null);

            prep.setInt(1, customer.getCustomerID());
            // prep.setInt(2, customer.getCustomerID());

            // prep.addBatch();
            cmc.getConnection().setAutoCommit(false);
            prep.executeUpdate();
            cmc.getConnection().setAutoCommit(true);

            prep = cmc.getConnection().prepareStatement(s1);
            prep.setInt(1, customer.getCustomerID());
            cmc.getConnection().setAutoCommit(false);
            prep.executeUpdate();
            cmc.getConnection().setAutoCommit(true);

        } catch (Exception e) {
            System.err.println(e);
            return false;
        }
        return true;

    }

    public List<CustomerMaster> getAllCustomer() {
        List<CustomerMaster> customers = new ArrayList<CustomerMaster>();

        try {
            Statement stat = cmc.getConnection().createStatement();
            ResultSet rs = stat.executeQuery("select * from customer_master;");
            CustomerMaster tempCustomer;
            while (rs.next()) {
                //System.out.println("name = " + rs.getString("customer_name"));
                int custID = rs.getInt(TableFiled.CUSTOMER_ID);
                tempCustomer = new CustomerMaster(custID, rs.getString(TableFiled.CUSTOMER_NAME), rs.getString(TableFiled.CUSTOMER_ADDRESS), rs.getString(TableFiled.CUSTOMER_CITY), rs.getString(TableFiled.CUSTOMER_STATE), rs.getString(TableFiled.CUSTOMER_ZIP), rs.getString(TableFiled.CUSTOMER_MOBILE), rs.getString(TableFiled.CUSTOMER_PHONE), rs.getBoolean(TableFiled.CUSTOMER_VISIBLE));
                tempCustomer.setCustomerSites(getCustomerOwnerSites(custID));
                customers.add(tempCustomer);
            }
            rs.close();
            //set customer siste;

        } catch (SQLException sql) {
            System.err.println(sql);
        }
        return customers;
    }
    public List<CustomerSite> getCustomerOwnerSites(int customerID){
        List<CustomerSite> tempCustomerSites = new ArrayList<CustomerSite>();
        for(CustomerSite cust : customerSite){
            if(cust.getCustomerID() == customerID){
                tempCustomerSites.add(cust);
            }
        }
        return tempCustomerSites;
    }
    public void setSite(){
        CustomerSiteDAO custSites = new CustomerSiteDAO(cmc);
        customerSite = custSites.getAllCustomerSites();
    }

    public CrushMetalConnection getCmc() {
        return cmc;
    }

    public void setCmc(CrushMetalConnection cmc) {
        this.cmc = cmc;
    }

    public List<CustomerSite> getCustomerSite() {
        return customerSite;
    }

    public void setCustomerSite(List<CustomerSite> customerSite) {
        this.customerSite = customerSite;
    }
    
}
