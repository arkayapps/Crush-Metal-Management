/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package crushmetalmanagement.dao;

/**
 *
 * @author Acer
 */
public class TableFiled {
    //Customer Master Filed
    public static final String CUSTOMER_ID = "customer_id";
    public static final String CUSTOMER_NAME = "customer_name";
    public static final String CUSTOMER_ADDRESS = "customer_address";
    public static final String CUSTOMER_CITY = "customer_city";
    public static final String CUSTOMER_STATE = "customer_state";
    public static final String CUSTOMER_ZIP = "customer_zip";
    public static final String CUSTOMER_MOBILE = "customer_mobile";
    public static final String CUSTOMER_PHONE= "customer_phone";
    public static final String CUSTOMER_VISIBLE = "isVisible";

    //Site Table Field
    public static final String SITE_ID = "site_id";
    public static final String SITE_NAME = "site_name";
    public static final String SITE_DESCRIPTION = "site_description";

    //Product Table Field
    public static final String PRODUCT_NAME_ID = "Product_name_id";
    public static final String PRODUCT_NAME = "product_name";

    // Product Unit Field
    public static final String PRD_UNIT_ID = "Prd_unit_id";
    public static final String PRD_UNIT_NAME = "Prd_unit_name";

    //Cash Register table filed
    public static final String CASHREGISTER_TABLE="cashRegister";
    public static final String CASH_REGISTER_ID="cashRegister_Id";
    public static final String CASH_TRAN_DATE="cashTran_Date";
    public static final String CASH_CHALLAN_NO="cashChallan_No";
    public static final String CASH_CUS_NAME="cashCus_Name";
    public static final String CASH_CUS_UNLOADING_PLACE="cashCusUnloading_Place";
    public static final String CASH_PRODUCTNAME_ID="productName_ID";
    public static final String CASH_TRAN_QTY="cashTran_Qty";
    public static final String CASH_PRODUCTUNIT_ID="productUnit_Id";
    public static final String CASH_PRODUCT_RATE="product_Rate";
    public static final String CASH_RECEIVEBY="cashReceive_By";
    public static final String CASH_TRAN_ROYALTY_MT="cashTranRoyalty_MT";
    public static final String CASH_VEHICLE_NO="cashVehicle_No";
    public static final String CASH_DRIVER_NAME="cashDriver_Name";
    public static final String CASH_TRAN_ROYALTY_NO="cashTranRoyalty_No";
    public static final String CASH_ROYALTY_RATE="cashRoyalty_Rate";
    public static final String CASH_ROYALTY_HOLDER_CUS_ID="royaltyHolderCus_ID";

    // Credit Register master
    public static final String CREDITREGISTER_TABLE="creditRegister";
    public static final String CREDIT_REGISTER_ID="credit_Register_Id";
    public static final String CREDIT_TRAN_DATE="creditTran_Date";
    public static final String CREDIT_CHALLAN_NO="creditChallan_No";
    public static final String CREDIT_CUS_ID="creditCus_Id";
    public static final String CREDIT_CUS_UNLOADING_PLACE="creditCusUnloading_Place";
    public static final String CREDIT_PRODUCT_NAME_ID="productName_ID";
    public static final String CREDIT_TRAN_QTY="creditTran_Qty";
    public static final String CREDIT_PRODUCT_UNIT_ID="productUnit_Id";
    public static final String CREDIT_PRODUCT_RATE="product_Rate";
    public static final String CREDIT_TRAN_ROYALTY_MT="creditTranRoyalty_MT";
    public static final String CREDIT_VEHICLE_NO="creditVehicle_No";
    public static final String CREDIT_DRIVER_NAME="creditDriver_Name";
    public static final String CREDIT_TRAN_ROYALTY_NO="creditTranRoyalty_No";
    public static final String CREDIT_ROYALTY_RATE="creditRoyalty_Rate";
    public static final String CREDIT_ROYALTY_HOLDER_CUS_ID="royaltyHolderCus_ID";
    public static final String CREDIT_CUST_SITE_ID="credit_cust_site_id";




}
