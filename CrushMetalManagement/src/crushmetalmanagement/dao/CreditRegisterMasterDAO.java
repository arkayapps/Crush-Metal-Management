package crushmetalmanagement.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import crushmetalmanagement.cmbeans.CreditRegisterMaster;
import crushmetalmanagement.cmbeans.CustomerMaster;

public class CreditRegisterMasterDAO {
	 CrushMetalConnection cmc;
         public CreditRegisterMasterDAO(CrushMetalConnection cmc){
             this.cmc = cmc;

         }
	 public boolean AddNewCreditRegisterInfo(CreditRegisterMaster creditRegisterMaster) {
		 try {
	            PreparedStatement prep = cmc.getConnection().prepareStatement("insert into " + TableFiled.CREDITREGISTER_TABLE + " values (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
	            prep.setLong(1, creditRegisterMaster.getCreditTranDate().getTime());
	            prep.setString(2, creditRegisterMaster.getCreditChallanNo());
	            prep.setInt(3, creditRegisterMaster.getCustomer().getCustomerID());
	            prep.setString(4, creditRegisterMaster.getCreditCusUnloadingPlace());
	            prep.setInt(5, creditRegisterMaster.getProductNameID());
	            prep.setDouble(6, creditRegisterMaster.getCreditTranQty());
	            prep.setInt(7, creditRegisterMaster.getProductUnitId());
	            prep.setDouble(8, creditRegisterMaster.getProductRate());
	            prep.setDouble(9, creditRegisterMaster.getCreditTranRoyaltyMT());
	            prep.setString(10, creditRegisterMaster.getCreditVehicleNo());
	            prep.setString(11, creditRegisterMaster.getCreditDriverName());
	            prep.setString(12, creditRegisterMaster.getCreditTranRoyaltyNo());
	            prep.setDouble(13, creditRegisterMaster.getCreditRoyaltyRate());
                    prep.setInt(14, creditRegisterMaster.getCreditSiteID());
	            prep.setInt(15, creditRegisterMaster.getRoyaltyHolderCusID());

	            prep.addBatch();
	            cmc.getConnection().setAutoCommit(false);
	            prep.executeBatch();
	            cmc.getConnection().setAutoCommit(true);
                    prep.close();
	        } catch (Exception e) {
	            System.err.println(e);
	            return false;
	        }
	        return true;
	 }
	 
	 public boolean updateCreditRegisterInfo(CreditRegisterMaster creditRegisterMaster) {
		 try {
	            PreparedStatement prep = cmc.getConnection().prepareStatement("update " + TableFiled.CREDITREGISTER_TABLE +" set "
	            			+ TableFiled.CREDIT_TRAN_DATE + " = ?,"
	            			+ TableFiled.CREDIT_CHALLAN_NO + " = ?, "
	            			+ TableFiled.CREDIT_CUS_ID + " = ?, "
	            			+ TableFiled.CREDIT_CUS_UNLOADING_PLACE + " = ?, "
	            			+ TableFiled.CREDIT_PRODUCT_NAME_ID +" = ?, "
	            			+ TableFiled.CREDIT_TRAN_QTY + " = ?, "
	            			+ TableFiled.CREDIT_PRODUCT_UNIT_ID + " = ?, "
	            			+ TableFiled.CREDIT_PRODUCT_RATE + " = ?, "
	            			+ TableFiled.CREDIT_TRAN_ROYALTY_MT + " = ?, "
	            			+ TableFiled.CREDIT_VEHICLE_NO +" = ?, "
	            			+ TableFiled.CREDIT_DRIVER_NAME + " = ?, "
	            			+ TableFiled.CREDIT_TRAN_ROYALTY_NO + " = ?, "
	            			+ TableFiled.CREDIT_ROYALTY_RATE + " = ?, "
	            			+ TableFiled.CREDIT_ROYALTY_HOLDER_CUS_ID + " = ? "
	            			+ " WHERE "
	            			+ TableFiled.CREDIT_REGISTER_ID +" = ? ");
	            prep.setLong(1, creditRegisterMaster.getCreditTranDate().getTime());
	            prep.setString(2, creditRegisterMaster.getCreditChallanNo());
	            prep.setInt(3, creditRegisterMaster.getCustomer().getCustomerID());
	            prep.setString(4, creditRegisterMaster.getCreditCusUnloadingPlace());
	            prep.setInt(5, creditRegisterMaster.getProductNameID());
	            prep.setDouble(6, creditRegisterMaster.getCreditTranQty());
	            prep.setInt(7, creditRegisterMaster.getProductUnitId());
	            prep.setDouble(8, creditRegisterMaster.getProductRate());
	            prep.setDouble(9, creditRegisterMaster.getCreditTranRoyaltyMT());
	            prep.setString(10, creditRegisterMaster.getCreditVehicleNo());
	            prep.setString(11, creditRegisterMaster.getCreditDriverName());
	            prep.setString(12, creditRegisterMaster.getCreditTranRoyaltyNo());
	            prep.setDouble(13, creditRegisterMaster.getCreditRoyaltyRate());
	            prep.setInt(14, creditRegisterMaster.getRoyaltyHolderCusID());
	            prep.setInt(15, creditRegisterMaster.getCreditRegisterId());
	            cmc.getConnection().setAutoCommit(false);
	            prep.executeUpdate();
	            cmc.getConnection().setAutoCommit(true);	            
	        } catch (Exception e) {
	            System.err.println(e);
	            return false;
	        }
	        return true;
	 }
	 
	 public boolean deleteCreditRegisterInfo(CreditRegisterMaster creditRegisterMaster) {
		 try {
	            PreparedStatement prep = cmc.getConnection().prepareStatement("delete from " + TableFiled.CREDITREGISTER_TABLE
	            			+ " WHERE "
	            			+ TableFiled.CREDIT_REGISTER_ID +" = ? ");
	            prep.setInt(1, creditRegisterMaster.getCreditRegisterId());
	            cmc.getConnection().setAutoCommit(false);
	            prep.executeUpdate();
	            cmc.getConnection().setAutoCommit(true);
		 	} catch (Exception e) {
	            System.err.println(e);
	            return false;
	        }
	        return true;
	 }

	 public List<CreditRegisterMaster> getAllCreditRegisterMaster() {
	        List<CreditRegisterMaster> creditRegisterMasterList = new ArrayList<CreditRegisterMaster>();
	        try {
	            Statement stat = cmc.getConnection().createStatement();
	           // ResultSet rs = stat.executeQuery("select * from "+TableFiled.CREDITREGISTER_TABLE);
                    String query ="SELECT * FROM "+TableFiled.CREDITREGISTER_TABLE+" as cr, customer_master as cm, customer_site cs where cr.creditCus_Id = cm.customer_id and cr.credit_cust_site_id = cs.site_id";
                    System.out.println(""+query);
	            ResultSet rs = stat.executeQuery(query);
                    CreditRegisterMaster creditRegisterMaster;
                    CustomerMaster tempCustomer;

	            while (rs.next()) {
	            	long date = rs.getLong(TableFiled.CREDIT_TRAN_DATE);
	            	Date newDate=new Date(date);
	            	int custID = rs.getInt(TableFiled.CREDIT_CUS_ID);
                        tempCustomer = new CustomerMaster(custID, rs.getString(TableFiled.CUSTOMER_NAME), rs.getString(TableFiled.CUSTOMER_ADDRESS), rs.getString(TableFiled.CUSTOMER_CITY), rs.getString(TableFiled.CUSTOMER_STATE), rs.getString(TableFiled.CUSTOMER_ZIP), rs.getString(TableFiled.CUSTOMER_MOBILE), rs.getString(TableFiled.CUSTOMER_PHONE), rs.getBoolean(TableFiled.CUSTOMER_VISIBLE));
                        //tempCustomer.setCustomerSites(getCustomerOwnerSites(custID));

                        creditRegisterMaster = new CreditRegisterMaster(rs.getInt(TableFiled.CREDIT_REGISTER_ID), newDate, rs.getString(TableFiled.CREDIT_CHALLAN_NO),tempCustomer, rs.getInt(TableFiled.CREDIT_CUST_SITE_ID),rs.getString(TableFiled.CREDIT_CUS_UNLOADING_PLACE), rs.getInt(TableFiled.CREDIT_PRODUCT_NAME_ID), rs.getDouble(TableFiled.CREDIT_TRAN_QTY), rs.getInt(TableFiled.CREDIT_PRODUCT_UNIT_ID), rs.getDouble(TableFiled.CREDIT_PRODUCT_RATE), rs.getDouble(TableFiled.CREDIT_TRAN_ROYALTY_MT), rs.getString(TableFiled.CREDIT_VEHICLE_NO), rs.getString(TableFiled.CREDIT_DRIVER_NAME), rs.getString(TableFiled.CREDIT_TRAN_ROYALTY_NO), rs.getDouble(TableFiled.CREDIT_ROYALTY_RATE), rs.getInt(TableFiled.CREDIT_ROYALTY_HOLDER_CUS_ID));
	            	creditRegisterMasterList.add(creditRegisterMaster);
	            }
	            rs.close();
	        } catch (SQLException sql) {
	            System.err.println(sql);
	        }
	        return creditRegisterMasterList;
	    }
}
