/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package crushmetalmanagement.dao;

import java.io.File;
import java.sql.*;

/**
 *
 * @author Acer
 */
public class CrushMetalConnection {

    private Connection connection;

    public CrushMetalConnection() {
        try {
            
            String dirPath = System.getenv("APPDATA");
            //dirPath =dirPath+"\\creshmetal";
            System.out.print(dirPath);
            //File f  = new File(dirPath);
            //f.createNewFile();
            Class.forName("org.sqlite.JDBC");

            connection = DriverManager.getConnection("jdbc:sqlite:"+dirPath+"\\database.db");
            //DriverManager.getConnection("jdbc:sqlite:test.db"); also we can user

            Statement stat = connection.createStatement();

            //create Customer Table if not exits
            stat.executeUpdate("CREATE TABLE if not exists customer_master (" + TableFiled.CUSTOMER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TableFiled.CUSTOMER_NAME + " TEXT(22), " + TableFiled.CUSTOMER_ADDRESS + " TEXT(100), " + TableFiled.CUSTOMER_CITY + " TEXT(50), " + TableFiled.CUSTOMER_STATE + " TEXT(50), " + TableFiled.CUSTOMER_ZIP + " TEXT(7), " + TableFiled.CUSTOMER_MOBILE + " TEXT(11), " + TableFiled.CUSTOMER_PHONE + " TEXT(15), " + TableFiled.CUSTOMER_VISIBLE + " Boolean);");
            String str = "CREATE TABLE if not exists customer_site (" + TableFiled.SITE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TableFiled.SITE_NAME + " TEXT, " + TableFiled.SITE_DESCRIPTION + " TEXT , " + TableFiled.CUSTOMER_ID + " INTEGER);";
            stat.executeUpdate(str);

            //Create ProductUnit Table if not eixts

            //Table is already exits;
            String strTbalExit = "SELECT name FROM sqlite_master WHERE type='"+"table"+"' AND name="+"'Product_unit'";
            PreparedStatement prep;
            prep = connection.prepareStatement(strTbalExit);
            ResultSet rs = prep.executeQuery();
            boolean isTableAlreadyExis = false;
            if(rs.next()){
                isTableAlreadyExis = true;
                System.out.println("available");
            }
            System.out.println(rs.getRow());
            System.out.println(strTbalExit);
            prep.close();
           // PreparedStatement prep;
            int tableNotCreateAlready = stat.executeUpdate("CREATE TABLE if not exists Product_unit (" + TableFiled.PRD_UNIT_ID + " Integer Primary Key, " + TableFiled.PRD_UNIT_NAME + " TEXT);");
            stat = connection.createStatement();

            if(!isTableAlreadyExis){
                try{
                    prep = connection.prepareStatement("insert into Product_unit values (?, ?);");
                    prep.setInt(1, 1);
                    prep.setString(2, "TON");
                    prep.addBatch();
                    prep.setInt(1, 2);
                    prep.setString(2, "CFT");
                    prep.addBatch();
                    prep.setInt(1, 3);
                    prep.setString(2, "CMT");
                    prep.addBatch();
                    connection.setAutoCommit(false);
                    prep.executeBatch();
                    connection.setAutoCommit(true);
                }catch(Exception e){
                    System.err.println(e);
                }
            }
            prep.close();
            tableNotCreateAlready = stat.executeUpdate("CREATE TABLE if not exists Product (" + TableFiled.PRODUCT_NAME_ID + " Integer Primary Key, " + TableFiled.PRODUCT_NAME + " TEXT);");
            System.out.println(tableNotCreateAlready);

            if(!isTableAlreadyExis){
                try{
                    prep = connection.prepareStatement("insert into Product values (?, ?);");
                    prep.setInt(1, 1);
                    prep.setString(2, "10M.M.");
                    prep.addBatch();
                    prep.setInt(1, 2);
                    prep.setString(2, "20M.M.");
                    prep.addBatch();
                    prep.setInt(1, 3);
                    prep.setString(2, "40M.M.");
                    prep.addBatch();
                    prep.setInt(1, 4);
                    prep.setString(2, "10M.M.");
                    prep.addBatch();
                    prep.setInt(1, 5);
                    prep.setString(2, "10-20M.M.");
                    prep.addBatch();
                    prep.setInt(1, 6);
                    prep.setString(2, "10-40M.M.");
                    prep.addBatch();
                    prep.setInt(1, 7);
                    prep.setString(2, "20-40M.M.");
                    prep.addBatch();
                    prep.setInt(1, 8);
                    prep.setString(2, "6M.M.");
                    prep.addBatch();
                    prep.setInt(1, 9);
                    prep.setString(2, "DUST");
                    prep.addBatch();
                    prep.setInt(1, 10);
                    prep.setString(2, "WMM");
                    prep.addBatch();
                    prep.setInt(1, 11);
                    prep.setString(2, "40/63");
                    prep.addBatch();
                    prep.setInt(1, 12);
                    prep.setString(2, "63/90");
                    prep.addBatch();
                    prep.setInt(1, 13);
                    prep.setString(2, "STONE");
                    prep.addBatch();
                    prep.setInt(1, 14);
                    prep.setString(2, "PUAR");
                    prep.addBatch();
                    connection.setAutoCommit(false);
                    prep.executeBatch();
                    connection.setAutoCommit(true);

                }catch(Exception e){
                    System.out.print(e);
                    //return;
                }
            }
            prep.close();
                 stat.executeUpdate("CREATE TABLE if not exists "+ TableFiled.CASHREGISTER_TABLE
            		+" ("+TableFiled.CASH_REGISTER_ID+" Integer Primary Key,"
            		+ TableFiled.CASH_TRAN_DATE + " LONG, "
            		+ TableFiled.CASH_CHALLAN_NO + " TEXT, "
            		+ TableFiled.CASH_CUS_NAME + " TEXT, "
            		+ TableFiled.CASH_CUS_UNLOADING_PLACE + " TEXT, "
            		+ TableFiled.CASH_PRODUCTNAME_ID + " INTEGER, "
            		+ TableFiled.CASH_TRAN_QTY + " LONG, "
            		+ TableFiled.CASH_PRODUCTUNIT_ID + " INTEGER, "
            		+ TableFiled.CASH_PRODUCT_RATE + " LONG, "
            		+ TableFiled.CASH_RECEIVEBY + " TEXT, "
            		+ TableFiled.CASH_TRAN_ROYALTY_MT + " LONG, "
            		+ TableFiled.CASH_VEHICLE_NO + " TEXT, "
            		+ TableFiled.CASH_DRIVER_NAME + " TEXT, "
            		+ TableFiled.CASH_TRAN_ROYALTY_NO + " TEXT,"
            		+ TableFiled.CASH_ROYALTY_RATE + " LONG, "
            		+ TableFiled.CASH_ROYALTY_HOLDER_CUS_ID + " INTEGER);");


            stat.executeUpdate("CREATE TABLE if not exists "+ TableFiled.CREDITREGISTER_TABLE
            		+" ("+TableFiled.CREDIT_REGISTER_ID+" Integer Primary Key,"
            		+ TableFiled.CREDIT_TRAN_DATE + " LONG, "
            		+ TableFiled.CREDIT_CHALLAN_NO+ " TEXT, "
            		+ TableFiled.CREDIT_CUS_ID + " INTEGER, "
            		+ TableFiled.CREDIT_CUS_UNLOADING_PLACE + " TEXT, "
            		+ TableFiled.CREDIT_PRODUCT_NAME_ID + " INTEGER, "
            		+ TableFiled.CREDIT_TRAN_QTY + " LONG, "
            		+ TableFiled.CREDIT_PRODUCT_UNIT_ID + " INTEGER, "
            		+ TableFiled.CREDIT_PRODUCT_RATE + " LONG, "
            		+ TableFiled.CREDIT_TRAN_ROYALTY_MT + " LONG, "
            		+ TableFiled.CREDIT_VEHICLE_NO + " TEXT, "
            		+ TableFiled.CREDIT_DRIVER_NAME + " TEXT, "
            		+ TableFiled.CREDIT_TRAN_ROYALTY_NO + " TEXT,"
            		+ TableFiled.CREDIT_ROYALTY_RATE + " LONG, "
                        + TableFiled.CREDIT_CUST_SITE_ID + " INTEGER, "
                        + TableFiled.CREDIT_ROYALTY_HOLDER_CUS_ID + " INTEGER);");


        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
