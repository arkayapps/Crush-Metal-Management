/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package crushmetalmanagement.dao;

import crushmetalmanagement.cmbeans.CustomerSite;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Acer
 */
public class CustomerSiteDAO {
    private CrushMetalConnection cmc;

    public CustomerSiteDAO(CrushMetalConnection cmc){
        this.cmc  = cmc;
    }
    public boolean addNewCustomerSite(CustomerSite customerSite) {
        try {
            PreparedStatement prep = cmc.getConnection().prepareStatement("insert into customer_site values (NULL, ?, ?, ?);");
            prep.setString(1, customerSite.getSiteName());
            prep.setString(2, customerSite.getSiteDescription());
            prep.setInt(3, customerSite.getCustomerID());
            prep.addBatch();
            cmc.getConnection().setAutoCommit(false);
            prep.executeBatch();
            cmc.getConnection().setAutoCommit(true);

        }catch(Exception e){
            
        }
        return true;
    }

    public List<CustomerSite> getAllCustomerSites(){
        List<CustomerSite> customerSites = new ArrayList<CustomerSite>();
        try {
            Statement stat = cmc.getConnection().createStatement();
            ResultSet rs = stat.executeQuery("select * from customer_site;");
            CustomerSite tempCustomerSite;
            while (rs.next()) {
                tempCustomerSite = new CustomerSite(rs.getInt(TableFiled.SITE_ID),rs.getString(TableFiled.SITE_NAME),rs.getString(TableFiled.SITE_DESCRIPTION),rs.getInt(TableFiled.CUSTOMER_ID));
                customerSites.add(tempCustomerSite);
            }
            rs.close();
        } catch (SQLException sql) {
            System.err.println(sql);
        }
        return customerSites;
    }
        public boolean UpdateCustomerSite(CustomerSite cusSite) {
        try{
            PreparedStatement prep=cmc.getConnection().prepareStatement("UPDATE customer_site SET "+TableFiled.SITE_NAME+"=?,"+TableFiled.SITE_DESCRIPTION+"=? WHERE "+TableFiled.SITE_ID+"=?;");
            prep.setString(1, cusSite.getSiteName());
            prep.setString(2, cusSite.getSiteDescription());
            prep.setInt(3, cusSite.getSiteID());
            prep.addBatch();
            cmc.getConnection().setAutoCommit(false);
            prep.executeBatch();
            cmc.getConnection().setAutoCommit(true);

        }catch(Exception e){

        }
        return true;
    }
    public boolean DeleteCustomerSite(CustomerSite cusSite) {
        
        try{
            System.out.println(cusSite.getSiteID());
            PreparedStatement prep=cmc.getConnection().prepareStatement("DELETE FROM customer_site WHERE "+TableFiled.SITE_ID+" = ?;");
            prep.setInt(1, cusSite.getSiteID());

           // prep.addBatch();
            cmc.getConnection().setAutoCommit(false);
            int noOfRowEffect = prep.executeUpdate();
            cmc.getConnection().setAutoCommit(true);

        }catch(Exception e){
            System.out.println(e);
        }

        return true;
    }
     public Boolean DeleteCustomerSite1(CustomerSite cusSite) {
         try{
            PreparedStatement prep=cmc.getConnection().prepareStatement("DELETE FROM customer_site WHERE "+TableFiled.SITE_ID+"=?;");
            prep.setInt(1, cusSite.getSiteID());

           // prep.addBatch();
            cmc.getConnection().setAutoCommit(false);
            int noOfRowEffect = prep.executeUpdate();
            cmc.getConnection().setAutoCommit(true);

        }catch(Exception e){

        }
        return true;
    }

}
