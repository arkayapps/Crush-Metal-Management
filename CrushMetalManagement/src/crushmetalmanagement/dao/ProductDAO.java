/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package crushmetalmanagement.dao;

import crushmetalmanagement.cmbeans.Product;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Acer
 */
public class ProductDAO {
    CrushMetalConnection cmc;

    public ProductDAO(CrushMetalConnection cmc){
        this.cmc = cmc;
    }
    public boolean addNewProduct(Product product){
        try{
         PreparedStatement prep = cmc.getConnection().prepareStatement("insert into Product values (NULL, ?);");
         prep.setString(2, product.getProductName());
         prep.addBatch();
         cmc.getConnection().setAutoCommit(false);
         prep.executeBatch();
         cmc.getConnection().setAutoCommit(true);

        }catch(Exception e){
            System.err.print(e);
            return false;
        }
        return true;
    }
    public List<Product> getAllProduct(){
        List<Product> products = new ArrayList<Product>();
        try {
            Statement stat = cmc.getConnection().createStatement();
            ResultSet rs = stat.executeQuery("select * from Product;");
            Product tempProduct;
            while(rs.next()){
                tempProduct = new Product(rs.getInt(TableFiled.PRODUCT_NAME_ID), rs.getString(TableFiled.PRODUCT_NAME));
                products.add(tempProduct);
            }

        }catch(SQLException sqle){
            System.err.println(sqle);
        }
        return products;
    }
}
