/*
 * CrushMetalManagementView.java
 */
package crushmetalmanagement;

import crushmetalmanagement.cmbeans.CustomerMaster;
import crushmetalmanagement.cmbeans.Product;
import crushmetalmanagement.cmbeans.ProductUnit;
import crushmetalmanagement.dao.CashRegisterMasterDAO;
import crushmetalmanagement.dao.CreditRegisterMasterDAO;
import crushmetalmanagement.dao.CrushMetalConnection;
import crushmetalmanagement.dao.CustomeMasterDAO;
import crushmetalmanagement.dao.CustomerSiteDAO;
import crushmetalmanagement.dao.ProductDAO;
import crushmetalmanagement.dao.ProductUnitDAO;
import crushmetalmanagement.reports.DailyChallanReport;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.util.List;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * The application's main frame.
 */
public class CrushMetalManagementView extends FrameView implements CrushMetalListener {

    public List<CustomerMaster> customers = new ArrayList<CustomerMaster>();
    public List<Product> products = new ArrayList<Product>();
    public List<ProductUnit> productsUnit = new ArrayList<ProductUnit>();
    public CrushMetalConnection cmc;

    public CustomeMasterDAO custmerDao;
    public ProductDAO productDao;
    public ProductUnitDAO productUnitDao;
    public CustomerSiteDAO customerSiteDao;
    public CashRegisterMasterDAO cashRegisterMasterDao;
    public CreditRegisterMasterDAO creditRegisterMasterDao;

    CusRegPan cusRegPan;
    CusInfoPan cusInfoPan;
    SiteInfoPan cusSiteInfoPan;
    SiteRegiPan cusSiteReg;
    CashRegisterPan cashRegister;
    CreditRegisterPan creditRegister;
    
    public CrushMetalManagementView(SingleFrameApplication app) {
        super(app);

        initComponents();
        cmc = new CrushMetalConnection();
        custmerDao = new CustomeMasterDAO(cmc);
        this.customers = custmerDao.getAllCustomer();
        System.out.println(customers.size());
        productDao = new ProductDAO(cmc);
        products = productDao.getAllProduct();
        System.out.println(products.size());
        productUnitDao = new ProductUnitDAO(cmc);
        productsUnit = productUnitDao.getAllProductUnit();
        System.out.println(productsUnit.size());
        customerSiteDao = new CustomerSiteDAO(cmc);
        cashRegisterMasterDao = new CashRegisterMasterDAO(cmc);
        creditRegisterMasterDao = new CreditRegisterMasterDAO(cmc);
        //setCustomers();

        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {

            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String) (evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer) (evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });
        cusInfoPan = new CusInfoPan(this);
        jTabbedPane1.add(cusInfoPan);
        jTabbedPane1.setTabComponentAt(0, new TabButton("Update Customer"));

    }

    public List<ProductUnit> getProductsUnit() {
        return productsUnit;
    }

    public void setProductsUnit(List<ProductUnit> productsUnit) {
        this.productsUnit = productsUnit;
    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = CrushMetalManagementApp.getApplication().getMainFrame();
            aboutBox = new CrushMetalManagementAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        CrushMetalManagementApp.getApplication().show(aboutBox);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        jButton4 = new javax.swing.JButton();
        jSeparator6 = new javax.swing.JToolBar.Separator();
        jButton2 = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        jButton3 = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JToolBar.Separator();
        jSeparator2 = new javax.swing.JSeparator();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        menuCashRegister = new javax.swing.JMenuItem();
        menuCreditRegister = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        menuReport = new javax.swing.JMenu();
        jMenuItem5 = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();
        jPopupMenu1 = new javax.swing.JPopupMenu();

        mainPanel.setName("mainPanel"); // NOI18N

        jToolBar1.setRollover(true);
        jToolBar1.setName("jToolBar1"); // NOI18N

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(crushmetalmanagement.CrushMetalManagementApp.class).getContext().getResourceMap(CrushMetalManagementView.class);
        jButton1.setIcon(resourceMap.getIcon("jButton1.icon")); // NOI18N
        jButton1.setToolTipText(resourceMap.getString("jButton1.toolTipText")); // NOI18N
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton1MouseEntered(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jSeparator3.setName("jSeparator3"); // NOI18N
        jToolBar1.add(jSeparator3);

        jButton4.setIcon(resourceMap.getIcon("jButton4.icon")); // NOI18N
        jButton4.setFocusable(false);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setName("jButton4"); // NOI18N
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton4MouseEntered(evt);
            }
        });
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);

        jSeparator6.setName("jSeparator6"); // NOI18N
        jToolBar1.add(jSeparator6);

        jButton2.setIcon(resourceMap.getIcon("jButton2.icon")); // NOI18N
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setName("jButton2"); // NOI18N
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton2MouseEntered(evt);
            }
        });
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jSeparator4.setName("jSeparator4"); // NOI18N
        jToolBar1.add(jSeparator4);

        jButton3.setIcon(resourceMap.getIcon("jButton3.icon")); // NOI18N
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setName("jButton3"); // NOI18N
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton3MouseEntered(evt);
            }
        });
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jSeparator5.setName("jSeparator5"); // NOI18N
        jToolBar1.add(jSeparator5);

        jSeparator2.setName("jSeparator2"); // NOI18N

        jTabbedPane1.setName("jTabbedPane1"); // NOI18N
        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });
        jTabbedPane1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTabbedPane1PropertyChange(evt);
            }
        });

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 764, Short.MAX_VALUE)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 764, Short.MAX_VALUE)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 744, Short.MAX_VALUE)
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE)
                .addContainerGap())
        );

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        menuCashRegister.setIcon(resourceMap.getIcon("menuCashRegister.icon")); // NOI18N
        menuCashRegister.setText(resourceMap.getString("menuCashRegister.text")); // NOI18N
        menuCashRegister.setName("menuCashRegister"); // NOI18N
        menuCashRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCashRegisterActionPerformed(evt);
            }
        });
        fileMenu.add(menuCashRegister);

        menuCreditRegister.setIcon(resourceMap.getIcon("menuCreditRegister.icon")); // NOI18N
        menuCreditRegister.setText(resourceMap.getString("menuCreditRegister.text")); // NOI18N
        menuCreditRegister.setName("menuCreditRegister"); // NOI18N
        menuCreditRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreditRegisterActionPerformed(evt);
            }
        });
        fileMenu.add(menuCreditRegister);

        jSeparator1.setName("jSeparator1"); // NOI18N
        fileMenu.add(jSeparator1);

        jMenuItem1.setText(resourceMap.getString("jMenuItem1.text")); // NOI18N
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItem1);

        jMenuItem2.setText(resourceMap.getString("jMenuItem2.text")); // NOI18N
        jMenuItem2.setName("jMenuItem2"); // NOI18N
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItem2);

        jMenuItem3.setText(resourceMap.getString("jMenuItem3.text")); // NOI18N
        jMenuItem3.setName("jMenuItem3"); // NOI18N
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItem3);

        jMenuItem4.setText(resourceMap.getString("jMenuItem4.text")); // NOI18N
        jMenuItem4.setName("jMenuItem4"); // NOI18N
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItem4);

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(crushmetalmanagement.CrushMetalManagementApp.class).getContext().getActionMap(CrushMetalManagementView.class, this);
        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setIcon(resourceMap.getIcon("exitMenuItem.icon")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        menuReport.setText(resourceMap.getString("menuReport.text")); // NOI18N
        menuReport.setName("menuReport"); // NOI18N

        jMenuItem5.setText(resourceMap.getString("jMenuItem5.text")); // NOI18N
        jMenuItem5.setName("jMenuItem5"); // NOI18N
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        menuReport.add(jMenuItem5);

        menuBar.add(menuReport);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setIcon(resourceMap.getIcon("aboutMenuItem.icon")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 764, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 594, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents

    private void menuCustomerRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCustomerRegisterActionPerformed
        // TODO add your handling code here:
        cusRegPan = new CusRegPan(this);
        cusRegPan.addCustomerListener(this);
        jTabbedPane1.add(cusRegPan);
        jTabbedPane1.setTabComponentAt(jTabbedPane1.getTabCount()-1, new TabButton("Add New Customer"));

    }//GEN-LAST:event_menuCustomerRegisterActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        boolean isAlready = false;
        int index=0;
        for(int i=0;i<jTabbedPane1.getTabCount();i++){
            if(jTabbedPane1.getComponentAt(i).getName().equalsIgnoreCase("CusRegPan")){
                isAlready = true;
                index = i;
            }
        }
        if(!isAlready){
            cusRegPan = new CusRegPan(this);
            jTabbedPane1.add(cusRegPan);
            System.out.println(jTabbedPane1.getTabCount());
            jTabbedPane1.setTabComponentAt(jTabbedPane1.getTabCount()-1, new TabButton("Add New Customer"));
            jTabbedPane1.setSelectedIndex(jTabbedPane1.getTabCount()-1);
        }else{
            jTabbedPane1.setSelectedIndex(index);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
         boolean isAlready = false;
        int index=0;
        for(int i=0;i<jTabbedPane1.getTabCount();i++){
            if(jTabbedPane1.getComponentAt(i).getName().equalsIgnoreCase("SiteRegiPan")){
                isAlready = true;
                index = i;
            }
        }
        if(!isAlready){
            cusSiteReg = new SiteRegiPan(this);
            jTabbedPane1.add(cusSiteReg);
            System.out.println(jTabbedPane1.getTabCount());
            jTabbedPane1.setTabComponentAt(jTabbedPane1.getTabCount()-1, new TabButton("Customer Site Info"));
            jTabbedPane1.setSelectedIndex(jTabbedPane1.getTabCount()-1);
        }else{
            jTabbedPane1.setSelectedIndex(index);
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseEntered
        // TODO add your handling code here:
        jButton1.setToolTipText("Add New Customer");
    }//GEN-LAST:event_jButton1MouseEntered

    private void jButton2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseEntered
        // TODO add your handling code here:
        jButton2.setToolTipText("Add New Cash Transection");
    }//GEN-LAST:event_jButton2MouseEntered

    private void jButton3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseEntered
        // TODO add your handling code here:
        jButton3.setToolTipText("Add New Credit Tansection");
    }//GEN-LAST:event_jButton3MouseEntered
    private void jButton4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseEntered
        // TODO add your handling code here:
        jButton4.setToolTipText("Add New Site");
    }//GEN-LAST:event_jButton4MouseEntered

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:

        boolean isAlready = false;
        int index=0;
        for(int i=0;i<jTabbedPane1.getTabCount();i++){
            if(jTabbedPane1.getComponentAt(i).getName().equalsIgnoreCase("CusRegPan")){
                isAlready = true;
                index = i;
            }
        }
        if(!isAlready){
            cusRegPan = new CusRegPan(this);
            jTabbedPane1.add(cusRegPan);
            System.out.println(jTabbedPane1.getTabCount());
            jTabbedPane1.setTabComponentAt(jTabbedPane1.getTabCount()-1, new TabButton("Add New Customer"));
            jTabbedPane1.setSelectedIndex(jTabbedPane1.getTabCount()-1);
        }else{
            jTabbedPane1.setSelectedIndex(index);
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
         boolean isAlready = false;
        int index=0;
        for(int i=0;i<jTabbedPane1.getTabCount();i++){
            if(jTabbedPane1.getComponentAt(i).getName().equalsIgnoreCase("CusInfoPan")){
                isAlready = true;
                index = i;
            }
        }
        if(!isAlready){
            cusInfoPan = new CusInfoPan(this);
            jTabbedPane1.add(cusInfoPan);
            System.out.println(jTabbedPane1.getTabCount());
            jTabbedPane1.setTabComponentAt(jTabbedPane1.getTabCount()-1, new TabButton("Add New Customer"));
            jTabbedPane1.setSelectedIndex(jTabbedPane1.getTabCount()-1);
        }else{
            jTabbedPane1.setSelectedIndex(index);
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jTabbedPane1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1PropertyChange
        // TODO add your handling code here:
        System.out.println("Chaneg");
    }//GEN-LAST:event_jTabbedPane1PropertyChange

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
         // TODO add your handling code here:
        System.out.println("Change");
         cusInfoPan.refreshScreen();
         if(cusSiteInfoPan!=null){
            cusSiteInfoPan.refreshScreen();
         }
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // TODO add your handling code here:
        boolean isAlready = false;
        int index=0;
        for(int i=0;i<jTabbedPane1.getTabCount();i++){
            if(jTabbedPane1.getComponentAt(i).getName().equalsIgnoreCase("CusSitePan")){
                isAlready = true;
                index = i;
            }
        }
        if(!isAlready){
            cusSiteInfoPan = new SiteInfoPan(this);
            jTabbedPane1.add(cusSiteInfoPan);
            System.out.println(jTabbedPane1.getTabCount());
            jTabbedPane1.setTabComponentAt(jTabbedPane1.getTabCount()-1, new TabButton("Customer Site Info"));
            jTabbedPane1.setSelectedIndex(jTabbedPane1.getTabCount()-1);
        }else{
            jTabbedPane1.setSelectedIndex(index);
        }
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        boolean isAlready = false;
        int index=0;
        for(int i=0;i<jTabbedPane1.getTabCount();i++){
            if(jTabbedPane1.getComponentAt(i).getName().equalsIgnoreCase("SiteRegiPan")){
                isAlready = true;
                index = i;
            }
        }
        if(!isAlready){
            cusSiteReg = new SiteRegiPan(this);
            jTabbedPane1.add(cusSiteReg);
            System.out.println(jTabbedPane1.getTabCount());
            jTabbedPane1.setTabComponentAt(jTabbedPane1.getTabCount()-1, new TabButton("Customer Site Info"));
            jTabbedPane1.setSelectedIndex(jTabbedPane1.getTabCount()-1);
        }else{
            jTabbedPane1.setSelectedIndex(index);
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void menuCashRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCashRegisterActionPerformed
        boolean isAlready = false;
        int index=0;
        for(int i=0;i<jTabbedPane1.getTabCount();i++){
            if(jTabbedPane1.getComponentAt(i).getName().equalsIgnoreCase("CashRegisterPan")){
                isAlready = true;
                index = i;
            }
        }
        if(!isAlready){
            cashRegister = new CashRegisterPan(this);
            jTabbedPane1.add(cashRegister);
            System.out.println(jTabbedPane1.getTabCount());
            jTabbedPane1.setTabComponentAt(jTabbedPane1.getTabCount()-1, new TabButton("Cash Register"));
            jTabbedPane1.setSelectedIndex(jTabbedPane1.getTabCount()-1);
        }else{
            jTabbedPane1.setSelectedIndex(index);
        }

    }//GEN-LAST:event_menuCashRegisterActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
                boolean isAlready = false;
        int index=0;
        for(int i=0;i<jTabbedPane1.getTabCount();i++){
            if(jTabbedPane1.getComponentAt(i).getName().equalsIgnoreCase("CashRegisterPan")){
                isAlready = true;
                index = i;
            }
        }
        if(!isAlready){
            cashRegister = new CashRegisterPan(this);
            jTabbedPane1.add(cashRegister);
            System.out.println(jTabbedPane1.getTabCount());
            jTabbedPane1.setTabComponentAt(jTabbedPane1.getTabCount()-1, new TabButton("Cash Register"));
            jTabbedPane1.setSelectedIndex(jTabbedPane1.getTabCount()-1);
        }else{
            jTabbedPane1.setSelectedIndex(index);
        }

    }//GEN-LAST:event_jButton2ActionPerformed

    private void menuCreditRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreditRegisterActionPerformed
        // TODO add your handling code here:
                // TODO add your handling code here:
        boolean isAlready = false;
        int index=0;
        for(int i=0;i<jTabbedPane1.getTabCount();i++){
            if(jTabbedPane1.getComponentAt(i).getName().equalsIgnoreCase("CreditRegisterPan")){
                isAlready = true;
                index = i;
            }
        }
        if(!isAlready){
            creditRegister = new CreditRegisterPan(this);
            jTabbedPane1.add(creditRegister);
            System.out.println(jTabbedPane1.getTabCount());
            jTabbedPane1.setTabComponentAt(jTabbedPane1.getTabCount()-1, new TabButton("Credit Register"));
            jTabbedPane1.setSelectedIndex(jTabbedPane1.getTabCount()-1);
        }else{
            jTabbedPane1.setSelectedIndex(index);
        }

    }//GEN-LAST:event_menuCreditRegisterActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        menuCreditRegisterActionPerformed(evt);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
        boolean isAlready = false;
        int index=0;
        for(int i=0;i<jTabbedPane1.getTabCount();i++){
            if(jTabbedPane1.getComponentAt(i).getName().equalsIgnoreCase("DailyChallanReport")){
                isAlready = true;
                index = i;
            }
        }
        if(!isAlready){
            DailyChallanReport dailayReport = new DailyChallanReport(this);
            jTabbedPane1.add(dailayReport);
            System.out.println(jTabbedPane1.getTabCount());
            jTabbedPane1.setTabComponentAt(jTabbedPane1.getTabCount()-1, new TabButton("Dailay Report"));
            jTabbedPane1.setSelectedIndex(jTabbedPane1.getTabCount()-1);
        }else{
            jTabbedPane1.setSelectedIndex(index);
        }
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JToolBar.Separator jSeparator6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuCashRegister;
    private javax.swing.JMenuItem menuCreditRegister;
    private javax.swing.JMenu menuReport;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    // End of variables declaration//GEN-END:variables
    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    private JDialog aboutBox;

    public List<CustomerMaster> getCustomers() {
        return this.customers;
    }

    public List<Product> getProducts() {
        return this.products;
    }

    public CustomeMasterDAO getCustomerMasterDAO() {
        return this.custmerDao;
    }

    public CrushMetalConnection getCrushMetalConnection() {
        return this.cmc;
    }

    public void setCustomers(List<CustomerMaster> customerMasters) {
        this.customers = customerMasters;
    }

    public CustomerSiteDAO getCustomerSiteDAO() {
        return customerSiteDao;
    }

    public List<ProductUnit> getProductUnits() {
        return productsUnit;
    }

    public CashRegisterMasterDAO getCashRegisterMasterDAO() {
        return cashRegisterMasterDao;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public CreditRegisterMasterDAO getCreditRegisterMasterDAO() {
        return creditRegisterMasterDao;
    }

    public String getProductName(int productID) {
        String productName = "";
        for(Product p : products){
            if(p.getProductNameId() == productID){
                productName = p.getProductName();
                break;
            }
        }
        return productName;
    }

    public String getProductUnitName(int productUnitID) {
        String productUnitName="";
        for(ProductUnit prodUnit : productsUnit){
            if(prodUnit.getPrdUnitId() == productUnitID){
                productUnitName = prodUnit.getPrdUnitName();
                break;
            }
        }
        return productUnitName;
    }

    class TabButton extends JPanel implements ActionListener {

        ImageIcon reg = null;
        ImageIcon over = null;

        public TabButton(String label) {

            super(new FlowLayout(FlowLayout.LEFT, 1, 1));
            add(new JLabel(label));
            try {

                reg = new javax.swing.ImageIcon(getClass().getResource("/crushmetalmanagement/resources/regular.JPG"));
                over = new javax.swing.ImageIcon(getClass().getResource("/crushmetalmanagement/resources/hoverOver.JPG"));

            } catch (Exception e) {

                e.printStackTrace();

            }
            setOpaque(false);
            final TabButton self = this;
            final JButton button = new JButton(reg);
            button.setOpaque(false);
            button.setRolloverIcon(over);
            button.setPressedIcon(over);
            button.setBorderPainted(false);
            button.setContentAreaFilled(false);
            button.addActionListener(this);
            button.setMargin(new Insets(1, 1, 1, 1));

            add(button);

        }

        public void actionPerformed(ActionEvent ae) {
            jTabbedPane1.remove(jTabbedPane1.indexOfTabComponent(this));

        }

    }
}
